﻿using System;
using System.Collections.Generic;
using System.Reflection.Emit;
using System.Text;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using SistemaConsorcio.Models;

namespace SistemaConsorcio.Data
{
    public class ApplicationDbContext : IdentityDbContext
    {
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options)
            : base(options)
        {
        }
        public DbSet<SistemaConsorcio.Models.TipoDepartamento> TipoDepartamento { get; set; }
        public DbSet<SistemaConsorcio.Models.Propietario> Propietario { get; set; }
        public DbSet<SistemaConsorcio.Models.Pago> Pago { get; set; }
        public DbSet<SistemaConsorcio.Models.General> General { get; set; }
        public DbSet<SistemaConsorcio.Models.Departamento> Departamento { get; set; }
        public DbSet<SistemaConsorcio.Models.RubroExpensa> RubroExpensa { get; set; }
        public DbSet<SistemaConsorcio.Models.Expensa> Expensa { get; set; }
        public DbSet<SistemaConsorcio.Models.PagoPendiente> PagoPendiente { get; set; }
        public DbSet<SistemaConsorcio.Models.Movimientos> Movimientos { get; set; }



    }
}
