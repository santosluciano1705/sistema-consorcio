﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace SistemaConsorcio.Data.Migrations
{
    public partial class movimientosimporte : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "RubroExpensaInformacion",
                table: "RubroExpensa");

            migrationBuilder.AlterColumn<DateTime>(
                name: "MovimientosVencimiento",
                table: "Movimientos",
                nullable: true,
                oldClrType: typeof(DateTime));

            migrationBuilder.AddColumn<bool>(
                name: "MovimientosInformacion",
                table: "Movimientos",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "MovimientosPagado",
                table: "Movimientos",
                nullable: false,
                defaultValue: false);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "MovimientosInformacion",
                table: "Movimientos");

            migrationBuilder.DropColumn(
                name: "MovimientosPagado",
                table: "Movimientos");

            migrationBuilder.AddColumn<bool>(
                name: "RubroExpensaInformacion",
                table: "RubroExpensa",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AlterColumn<DateTime>(
                name: "MovimientosVencimiento",
                table: "Movimientos",
                nullable: false,
                oldClrType: typeof(DateTime),
                oldNullable: true);
        }
    }
}
