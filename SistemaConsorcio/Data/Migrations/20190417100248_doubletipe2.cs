﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace SistemaConsorcio.Data.Migrations
{
    public partial class doubletipe2 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<decimal>(
                name: "TipoDepartamentoCoeficiente",
                table: "TipoDepartamento",
                nullable: false,
                oldClrType: typeof(double));
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<double>(
                name: "TipoDepartamentoCoeficiente",
                table: "TipoDepartamento",
                nullable: false,
                oldClrType: typeof(decimal));
        }
    }
}
