﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace SistemaConsorcio.Data.Migrations
{
    public partial class generalv2 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "GeneralMailFrom",
                table: "General",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "GeneralMailPassword",
                table: "General",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "GeneralMailFrom",
                table: "General");

            migrationBuilder.DropColumn(
                name: "GeneralMailPassword",
                table: "General");
        }
    }
}
