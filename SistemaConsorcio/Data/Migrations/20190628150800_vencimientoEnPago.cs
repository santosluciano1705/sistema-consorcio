﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace SistemaConsorcio.Data.Migrations
{
    public partial class vencimientoEnPago : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<decimal>(
                name: "PagoPendientePrimerVencimiento",
                table: "PagoPendiente",
                nullable: false,
                defaultValue: 0m);

            migrationBuilder.AddColumn<decimal>(
                name: "PagoPendienteSegundoVencimiento",
                table: "PagoPendiente",
                nullable: false,
                defaultValue: 0m);

            migrationBuilder.AddColumn<decimal>(
                name: "PagoPendienteTercerVencimiento",
                table: "PagoPendiente",
                nullable: false,
                defaultValue: 0m);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "PagoPendientePrimerVencimiento",
                table: "PagoPendiente");

            migrationBuilder.DropColumn(
                name: "PagoPendienteSegundoVencimiento",
                table: "PagoPendiente");

            migrationBuilder.DropColumn(
                name: "PagoPendienteTercerVencimiento",
                table: "PagoPendiente");
        }
    }
}
