﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace SistemaConsorcio.Data.Migrations
{
    public partial class saldoDeptos : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Pago_Expensa_ExpensaId",
                table: "Pago");

            migrationBuilder.DropIndex(
                name: "IX_Pago_ExpensaId",
                table: "Pago");

            migrationBuilder.DropColumn(
                name: "ExpensaId",
                table: "Pago");

            migrationBuilder.DropColumn(
                name: "MovimientosVencimiento",
                table: "Movimientos");

            migrationBuilder.AddColumn<decimal>(
                name: "DepartamentoSaldo",
                table: "Departamento",
                nullable: false,
                defaultValue: 0m);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "DepartamentoSaldo",
                table: "Departamento");

            migrationBuilder.AddColumn<int>(
                name: "ExpensaId",
                table: "Pago",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<DateTime>(
                name: "MovimientosVencimiento",
                table: "Movimientos",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Pago_ExpensaId",
                table: "Pago",
                column: "ExpensaId");

            migrationBuilder.AddForeignKey(
                name: "FK_Pago_Expensa_ExpensaId",
                table: "Pago",
                column: "ExpensaId",
                principalTable: "Expensa",
                principalColumn: "ExpensaId",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
