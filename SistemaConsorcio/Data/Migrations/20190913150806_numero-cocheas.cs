﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace SistemaConsorcio.Data.Migrations
{
    public partial class numerococheas : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<long>(
                name: "PropietarioTelefono",
                table: "Propietario",
                nullable: true,
                oldClrType: typeof(long));

            migrationBuilder.AddColumn<int>(
                name: "DepartamentoCantidadCochera",
                table: "Departamento",
                nullable: false,
                defaultValue: 0);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "DepartamentoCantidadCochera",
                table: "Departamento");

            migrationBuilder.AlterColumn<long>(
                name: "PropietarioTelefono",
                table: "Propietario",
                nullable: false,
                oldClrType: typeof(long),
                oldNullable: true);
        }
    }
}
