﻿using System;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace SistemaConsorcio.Data.Migrations
{
    public partial class ModelosCreados : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Movimientos",
                columns: table => new
                {
                    MovimientosId = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    MovimientosDescripcion = table.Column<string>(nullable: true),
                    MovimientosVencimiento = table.Column<DateTime>(nullable: false),
                    MovimientosImporte = table.Column<decimal>(nullable: false),
                    RubroExpensaId = table.Column<int>(nullable: false),
                    ExpensaId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Movimientos", x => x.MovimientosId);
                    table.ForeignKey(
                        name: "FK_Movimientos_Expensa_ExpensaId",
                        column: x => x.ExpensaId,
                        principalTable: "Expensa",
                        principalColumn: "ExpensaId",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Movimientos_RubroExpensa_RubroExpensaId",
                        column: x => x.RubroExpensaId,
                        principalTable: "RubroExpensa",
                        principalColumn: "RubroExpensaId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "PagoPendiente",
                columns: table => new
                {
                    PagoPendienteId = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    ExpensaId = table.Column<int>(nullable: false),
                    DepartamentoId = table.Column<int>(nullable: false),
                    PagoPendienteBono = table.Column<byte[]>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PagoPendiente", x => x.PagoPendienteId);
                    table.ForeignKey(
                        name: "FK_PagoPendiente_Departamento_DepartamentoId",
                        column: x => x.DepartamentoId,
                        principalTable: "Departamento",
                        principalColumn: "DepartamentoId",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_PagoPendiente_Expensa_ExpensaId",
                        column: x => x.ExpensaId,
                        principalTable: "Expensa",
                        principalColumn: "ExpensaId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Movimientos_ExpensaId",
                table: "Movimientos",
                column: "ExpensaId");

            migrationBuilder.CreateIndex(
                name: "IX_Movimientos_RubroExpensaId",
                table: "Movimientos",
                column: "RubroExpensaId");

            migrationBuilder.CreateIndex(
                name: "IX_PagoPendiente_DepartamentoId",
                table: "PagoPendiente",
                column: "DepartamentoId");

            migrationBuilder.CreateIndex(
                name: "IX_PagoPendiente_ExpensaId",
                table: "PagoPendiente",
                column: "ExpensaId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Movimientos");

            migrationBuilder.DropTable(
                name: "PagoPendiente");
        }
    }
}
