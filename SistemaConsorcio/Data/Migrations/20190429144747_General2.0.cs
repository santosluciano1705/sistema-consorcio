﻿using System;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace SistemaConsorcio.Data.Migrations
{
    public partial class General20 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Pago_Propietario_PropietarioId",
                table: "Pago");

            migrationBuilder.DropColumn(
                name: "Periodo",
                table: "Pago");

            migrationBuilder.RenameColumn(
                name: "PropietarioId",
                table: "Pago",
                newName: "ExpensaId");

            migrationBuilder.RenameColumn(
                name: "PagoVencimiento",
                table: "Pago",
                newName: "DepartamentoId");

            migrationBuilder.RenameIndex(
                name: "IX_Pago_PropietarioId",
                table: "Pago",
                newName: "IX_Pago_ExpensaId");

            migrationBuilder.AddColumn<decimal>(
                name: "GeneralPrimerVencimiento",
                table: "General",
                nullable: false,
                defaultValue: 0m);

            migrationBuilder.AddColumn<decimal>(
                name: "GeneralSegundoVencimiento",
                table: "General",
                nullable: false,
                defaultValue: 0m);

            migrationBuilder.AddColumn<decimal>(
                name: "GeneralTercerVencimiento",
                table: "General",
                nullable: false,
                defaultValue: 0m);

            migrationBuilder.CreateTable(
                name: "Expensa",
                columns: table => new
                {
                    ExpensaId = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    ExpensaPdf = table.Column<byte[]>(nullable: true),
                    Periodo = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Expensa", x => x.ExpensaId);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Pago_DepartamentoId",
                table: "Pago",
                column: "DepartamentoId");

            migrationBuilder.AddForeignKey(
                name: "FK_Pago_Departamento_DepartamentoId",
                table: "Pago",
                column: "DepartamentoId",
                principalTable: "Departamento",
                principalColumn: "DepartamentoId",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Pago_Expensa_ExpensaId",
                table: "Pago",
                column: "ExpensaId",
                principalTable: "Expensa",
                principalColumn: "ExpensaId",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Pago_Departamento_DepartamentoId",
                table: "Pago");

            migrationBuilder.DropForeignKey(
                name: "FK_Pago_Expensa_ExpensaId",
                table: "Pago");

            migrationBuilder.DropTable(
                name: "Expensa");

            migrationBuilder.DropIndex(
                name: "IX_Pago_DepartamentoId",
                table: "Pago");

            migrationBuilder.DropColumn(
                name: "GeneralPrimerVencimiento",
                table: "General");

            migrationBuilder.DropColumn(
                name: "GeneralSegundoVencimiento",
                table: "General");

            migrationBuilder.DropColumn(
                name: "GeneralTercerVencimiento",
                table: "General");

            migrationBuilder.RenameColumn(
                name: "ExpensaId",
                table: "Pago",
                newName: "PropietarioId");

            migrationBuilder.RenameColumn(
                name: "DepartamentoId",
                table: "Pago",
                newName: "PagoVencimiento");

            migrationBuilder.RenameIndex(
                name: "IX_Pago_ExpensaId",
                table: "Pago",
                newName: "IX_Pago_PropietarioId");

            migrationBuilder.AddColumn<DateTime>(
                name: "Periodo",
                table: "Pago",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddForeignKey(
                name: "FK_Pago_Propietario_PropietarioId",
                table: "Pago",
                column: "PropietarioId",
                principalTable: "Propietario",
                principalColumn: "PropietarioId",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
