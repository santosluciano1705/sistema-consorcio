﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace SistemaConsorcio.Data.Migrations
{
    public partial class Propietario3 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Propietario_Departamento_DepartamentoId",
                table: "Propietario");

            migrationBuilder.DropIndex(
                name: "IX_Propietario_DepartamentoId",
                table: "Propietario");

            migrationBuilder.DropColumn(
                name: "DepartamentoId",
                table: "Propietario");

            migrationBuilder.DropColumn(
                name: "PorpietarioPiso",
                table: "Propietario");

            migrationBuilder.DropColumn(
                name: "PropietarioCochera",
                table: "Propietario");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "DepartamentoId",
                table: "Propietario",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "PorpietarioPiso",
                table: "Propietario",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<bool>(
                name: "PropietarioCochera",
                table: "Propietario",
                nullable: false,
                defaultValue: false);

            migrationBuilder.CreateIndex(
                name: "IX_Propietario_DepartamentoId",
                table: "Propietario",
                column: "DepartamentoId");

            migrationBuilder.AddForeignKey(
                name: "FK_Propietario_Departamento_DepartamentoId",
                table: "Propietario",
                column: "DepartamentoId",
                principalTable: "Departamento",
                principalColumn: "DepartamentoId",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
