﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace SistemaConsorcio.Data.Migrations
{
    public partial class generalespendientes : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<DateTime>(
                name: "PagoPendienteFechaPrimerVencimiento",
                table: "PagoPendiente",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<DateTime>(
                name: "PagoPendienteFechaSegundoVencimiento",
                table: "PagoPendiente",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<DateTime>(
                name: "PagoPendienteFechaTercerVencimiento",
                table: "PagoPendiente",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<string>(
                name: "GeneralDireccionAdministracion",
                table: "General",
                nullable: true);

            migrationBuilder.AddColumn<long>(
                name: "GeneralTelefonoAdministracion",
                table: "General",
                nullable: false,
                defaultValue: 0L);

            migrationBuilder.AddColumn<int>(
                name: "GeneralTelefonoAdministracionCodigo",
                table: "General",
                nullable: false,
                defaultValue: 0);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "PagoPendienteFechaPrimerVencimiento",
                table: "PagoPendiente");

            migrationBuilder.DropColumn(
                name: "PagoPendienteFechaSegundoVencimiento",
                table: "PagoPendiente");

            migrationBuilder.DropColumn(
                name: "PagoPendienteFechaTercerVencimiento",
                table: "PagoPendiente");

            migrationBuilder.DropColumn(
                name: "GeneralDireccionAdministracion",
                table: "General");

            migrationBuilder.DropColumn(
                name: "GeneralTelefonoAdministracion",
                table: "General");

            migrationBuilder.DropColumn(
                name: "GeneralTelefonoAdministracionCodigo",
                table: "General");
        }
    }
}
