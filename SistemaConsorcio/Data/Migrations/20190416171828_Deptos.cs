﻿using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace SistemaConsorcio.Data.Migrations
{
    public partial class Deptos : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Departamento",
                columns: table => new
                {
                    DepartamentoId = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    TipoDepartamentoId = table.Column<int>(nullable: false),
                    DepartamentoPiso = table.Column<int>(nullable: false),
                    PropietarioId = table.Column<int>(nullable: false),
                    DepartamentoCochera = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Departamento", x => x.DepartamentoId);
                    table.ForeignKey(
                        name: "FK_Departamento_Propietario_PropietarioId",
                        column: x => x.PropietarioId,
                        principalTable: "Propietario",
                        principalColumn: "PropietarioId",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Departamento_TipoDepartamento_TipoDepartamentoId",
                        column: x => x.TipoDepartamentoId,
                        principalTable: "TipoDepartamento",
                        principalColumn: "TipoDepartamentoId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Departamento_PropietarioId",
                table: "Departamento",
                column: "PropietarioId");

            migrationBuilder.CreateIndex(
                name: "IX_Departamento_TipoDepartamentoId",
                table: "Departamento",
                column: "TipoDepartamentoId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Departamento");
        }
    }
}
