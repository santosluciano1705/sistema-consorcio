﻿using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace SistemaConsorcio.Data.Migrations
{
    public partial class Propietario : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Propietario",
                columns: table => new
                {
                    PropietarioId = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    PropietarioNombre = table.Column<string>(nullable: false),
                    PorpietarioPiso = table.Column<int>(nullable: false),
                    DepartamentoId = table.Column<int>(nullable: false),
                    PropietarioCuil = table.Column<int>(nullable: false),
                    PropietarioCochera = table.Column<bool>(nullable: false),
                    PropietarioEmail = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Propietario", x => x.PropietarioId);
                    table.ForeignKey(
                        name: "FK_Propietario_Departamento_DepartamentoId",
                        column: x => x.DepartamentoId,
                        principalTable: "Departamento",
                        principalColumn: "DepartamentoId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Propietario_DepartamentoId",
                table: "Propietario",
                column: "DepartamentoId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Propietario");
        }
    }
}
