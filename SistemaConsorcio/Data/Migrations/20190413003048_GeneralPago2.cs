﻿using System;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace SistemaConsorcio.Data.Migrations
{
    public partial class GeneralPago2 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "General",
                columns: table => new
                {
                    GeneralId = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    GeneralNombreConsorcio = table.Column<string>(nullable: true),
                    GeneralNombreCuentaBancaria = table.Column<string>(nullable: true),
                    GeneralPorcentajeVencimiento = table.Column<decimal>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_General", x => x.GeneralId);
                });

            migrationBuilder.CreateTable(
                name: "Pago",
                columns: table => new
                {
                    PagoId = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    PagoFecha = table.Column<DateTime>(nullable: false),
                    PropietarioId = table.Column<int>(nullable: false),
                    Periodo = table.Column<DateTime>(nullable: false),
                    PagoImporte = table.Column<decimal>(nullable: false),
                    PagoVencimiento = table.Column<int>(nullable: false),
                    PagoForma = table.Column<string>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Pago", x => x.PagoId);
                    table.ForeignKey(
                        name: "FK_Pago_Propietario_PropietarioId",
                        column: x => x.PropietarioId,
                        principalTable: "Propietario",
                        principalColumn: "PropietarioId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Pago_PropietarioId",
                table: "Pago",
                column: "PropietarioId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "General");

            migrationBuilder.DropTable(
                name: "Pago");
        }
    }
}
