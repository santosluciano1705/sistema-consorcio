﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace SistemaConsorcio.Data.Migrations
{
    public partial class General1 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "GeneralPorcentajeVencimiento",
                table: "General");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<decimal>(
                name: "GeneralPorcentajeVencimiento",
                table: "General",
                nullable: false,
                defaultValue: 0m);
        }
    }
}
