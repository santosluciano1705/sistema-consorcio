﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace SistemaConsorcio.Data.Migrations
{
    public partial class generalnuevo : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<decimal>(
                name: "GeneralPorcentajeCochera",
                table: "General",
                nullable: false,
                defaultValue: 0m);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "GeneralPorcentajeCochera",
                table: "General");
        }
    }
}
