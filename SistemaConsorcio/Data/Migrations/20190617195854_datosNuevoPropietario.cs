﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace SistemaConsorcio.Data.Migrations
{
    public partial class datosNuevoPropietario : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "PropietarioDireccion",
                table: "Propietario",
                nullable: false,
                defaultValue: "");

            migrationBuilder.AddColumn<long>(
                name: "PropietarioTelefono",
                table: "Propietario",
                nullable: false,
                defaultValue: 0L);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "PropietarioDireccion",
                table: "Propietario");

            migrationBuilder.DropColumn(
                name: "PropietarioTelefono",
                table: "Propietario");
        }
    }
}
