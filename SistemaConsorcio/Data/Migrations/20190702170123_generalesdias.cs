﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace SistemaConsorcio.Data.Migrations
{
    public partial class generalesdias : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "GeneralDiaPrimerVencimiento",
                table: "General",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "GeneralDiaSegundoVencimiento",
                table: "General",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "GeneralDiaTercerVencimiento",
                table: "General",
                nullable: false,
                defaultValue: 0);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "GeneralDiaPrimerVencimiento",
                table: "General");

            migrationBuilder.DropColumn(
                name: "GeneralDiaSegundoVencimiento",
                table: "General");

            migrationBuilder.DropColumn(
                name: "GeneralDiaTercerVencimiento",
                table: "General");
        }
    }
}
