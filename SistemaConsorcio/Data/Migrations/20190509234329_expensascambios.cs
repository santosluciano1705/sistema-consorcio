﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace SistemaConsorcio.Data.Migrations
{
    public partial class expensascambios : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "PeriodoFin",
                table: "Expensa");

            migrationBuilder.RenameColumn(
                name: "PeriodoInicio",
                table: "Expensa",
                newName: "ExpensaPeriodo");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "ExpensaPeriodo",
                table: "Expensa",
                newName: "PeriodoInicio");

            migrationBuilder.AddColumn<DateTime>(
                name: "PeriodoFin",
                table: "Expensa",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));
        }
    }
}
