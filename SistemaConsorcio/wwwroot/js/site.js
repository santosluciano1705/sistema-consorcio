﻿// Please see documentation at https://docs.microsoft.com/aspnet/core/client-side/bundling-and-minification
// for details on configuring this project to bundle and minify static web assets.

// Write your JavaScript code.

$('document').ready(function (e) {
    alertify.set('notifier', 'position', 'top-center');
    //eliminar con partial render
    $('body').on('click', '.btn-delete-ajax', function (event) {
        event.preventDefault();
        alertify.confirm('Alerta!', 'Esta seguro que quiere eliminar?',
            () => {
                $.ajax({
                    url: $(this).data('action')+'/' + $(this).data('id'),
                    type: 'Get',
                    success: (data) => {
                        if (data.eliminado) {
                            $(this).parent().parent().remove();
                            alertify.success(data.msg);
                        }
                        else {
                            alertify.error(data.msg);
                        }
                    },
                    error: function (err) {
                        alertify.error("Error: " + err.msg);
                    }
                });
            }
            , function () {
                alertify.message('Cancelado');
            }).set('labels', { ok: 'SI', cancel: 'NO' });
    });
    //llamar a index con partial render
    $('body').on('click', '.btn-index-ajax', function (event) {
        event.preventDefault();
        $('.btn-index-ajax.active').removeClass('active');
        $(this).addClass('active');
        $('.contenedor').html('<i class="fas fa-circle-notch fa-spin carga"></i>');
        $('.contenedor').load($(this).data('action'));
    });

    $('body').on('click', '.btn-index-ajax-nomenu', function (event) {
        event.preventDefault();
        $('.contenedor').html('<i class="fas fa-circle-notch fa-spin carga"></i>');
        $('.contenedor').load($(this).data('action'));
    });

    $("#menu-toggle").click(function (e) {
        e.preventDefault();
        $("#wrapper").toggleClass("toggled");
    });

    $('body').on('click', '.btn-modal', function () {
        $('.contain-CU').html('<i class="fas fa-circle-notch fa-spin carga"></i>');
        $('.contain-CU').load($(this).data('action'));
    });
    $('.home-btn').click();
});
