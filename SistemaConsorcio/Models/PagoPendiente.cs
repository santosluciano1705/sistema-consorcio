﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace SistemaConsorcio.Models
{
    public class PagoPendiente
    {
        public int PagoPendienteId { get; set; }
        public int ExpensaId { get; set; }
        public Expensa PagoPendienteExpensa { get; set; }
        public int DepartamentoId { get; set; }
        public Departamento PagoPendienteDepartamento { get; set; }
        public decimal PagoPendienteImporte { get; set; }
        public decimal PagoPendientePrimerVencimiento { get; set; }
        public decimal PagoPendienteSegundoVencimiento { get; set; }
        public decimal PagoPendienteTercerVencimiento { get; set; }
        [DataType(DataType.Date)]
        public DateTime PagoPendienteFechaPrimerVencimiento { get; set; }
        [DataType(DataType.Date)]
        public DateTime PagoPendienteFechaSegundoVencimiento { get; set; }
        [DataType(DataType.Date)]
        public DateTime PagoPendienteFechaTercerVencimiento { get; set; }
        public byte[] PagoPendienteBono { get; set; }
        public string PagoPendienteDescripcion { get; set; }
    }
}
