﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SistemaConsorcio.Models
{
    public class ReporteCalculoDepartamentos
    {
        public decimal total { get; set; }
        public List<Departamento> departamentos { get; set; }
        public General general { get; set; }
        public Expensa expensa { get; set; }
        public int cantidadCocheras { get; set; }
    }
}
