﻿using System.ComponentModel.DataAnnotations;

namespace SistemaConsorcio.Models
{
    public class Departamento
    {
        public int DepartamentoId { get; set; }
        [Required(ErrorMessage = "El departamento debe tener una letra asociada")]
        [Display(Name = "Letra")]
        public int TipoDepartamentoId { get; set; }
        [Display(Name = "Letra")]
        public TipoDepartamento DepartamentoTipo { get; set; }
        [Required(ErrorMessage = "Debe ingresar el piso en el que se encuentra el departamento")]
        [Range(0, 99, ErrorMessage = "El valor {0} no se encuentra dentro los valores permitidos")]
        [Display(Name = "Piso")]
        public int DepartamentoPiso { get; set; }
        [Required(ErrorMessage = "El departamento debe tener un propietario")]
        [Display(Name = "Propietario")]
        public int PropietarioId { get; set; }
        [Display(Name = "Propietario")]
        public Propietario DepartamentoPropietario { get; set; }
        [Display(Name = "Tiene cochera?")]
        public bool DepartamentoCochera { get; set; }
        [Display(Name = "Saldo")]
        public decimal DepartamentoSaldo { get; set; }
        [Display(Name = "Cantidad")]
        public int DepartamentoCantidadCochera { get; set; }
        [Display(Name = "Sufijo")]
        public int DepartamentoSufix { get; set; }
    }
}


