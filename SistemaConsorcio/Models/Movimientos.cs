﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace SistemaConsorcio.Models
{
    public class Movimientos
    {
        public int MovimientosId { get; set; }
        [Display(Name = "Descripcion")]
        public string MovimientosDescripcion { get; set; }
        [Display(Name = "Pagado?")]
        public bool MovimientosPagado { get; set; }
        [Display(Name = "Importe")]
        [Required(ErrorMessage = "Debe ingresar un importe")]
        public decimal MovimientosImporte { get; set; }
        [Display(Name = "Rubro")]
        public int RubroExpensaId { get; set; }
        [Display(Name = "Rubro")]
        public RubroExpensa MovimientosRubro { get; set; }
        public int ExpensaId { get; set; }
        public Expensa MovimientosExpensa { get; set; }
        [Display(Name = "Es informacion economica?")]
        public bool MovimientosInformacion { get; set; }//indica si es info economica para saber donde va, y si tiene vencimiento

    }
}
