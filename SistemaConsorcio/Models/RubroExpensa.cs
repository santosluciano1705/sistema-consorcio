﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace SistemaConsorcio.Models
{
    public class RubroExpensa
    {
        public int RubroExpensaId { get; set; }
        [Display(Name = "Descripcion del rubro")]
        [Required (ErrorMessage = "Debe ingresar una descripcion")]
        public string RubroExpensaDescripcion { get; set; }
    }
}
