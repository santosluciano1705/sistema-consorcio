﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace SistemaConsorcio.Models
{
    public class Expensa
    {
        public int ExpensaId { get; set; }
        public byte[] ExpensaPdf { get; set; }
        [Required]
        [DataType(DataType.Date)]
        [Display(Name = "Periodo de expensa")]
        public DateTime ExpensaPeriodo { get; set; }
        public bool ExpensaConfirmada { get; set; }
        public decimal ExpensaReserva { get; set; }
    }
}
