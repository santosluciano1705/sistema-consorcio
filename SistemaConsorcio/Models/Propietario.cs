﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace SistemaConsorcio.Models
{
    public class Propietario
    {
        public int PropietarioId { get; set; }
        [Required(ErrorMessage ="Debe ingresar un apellido y nombre")]
        [Display(Name = "Apellido y Nombre")]
        public string PropietarioNombre { get; set; }
        [Required(ErrorMessage = "El propietario debe tener un cuil")]
        [Display(Name = "CUIL (Ingrese el numero sin guiones)")]
        [Range(9999999999,99999999999,ErrorMessage = "Ingrese un cuil valido")]
        public long PropietarioCuil { get; set; }
        [Display(Name = "Email")]
        [EmailAddress(ErrorMessage = "Ingrese un mail valido")] 
        public string PropietarioEmail { get; set; }
        [Display(Name = "Enviar mails?")]
        public bool PropietarioSuscripto { get; set; }
        [Display (Name = "Dirección")]
        [Required(ErrorMessage = "La direccion es obligatoria")]
        public string PropietarioDireccion { get; set; }
        [Display (Name = "Telefono")]
        public Int64? PropietarioTelefono { get; set; }
    }
}
