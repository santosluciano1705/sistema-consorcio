﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace SistemaConsorcio.Models
{
    public class Pago
    {
        public int PagoId { get; set; }
        [Required(ErrorMessage = "Ingrese la fecha del pago")]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:dd/MM/yyyy}")]
        [Display(Name = "Fecha del pago")]
        [DataType(DataType.Date)]
        public DateTime PagoFecha { get; set; }
        [Required(ErrorMessage = "Seleccione un departamento")]
        [Display(Name = "Departamento")]
        public int DepartamentoId { get; set; }
        [Display(Name = "Departamento")]
        public Departamento PagoDepartamento { get; set; }
        [Display(Name = "Importe")]
        [Required(ErrorMessage = "Ingrese un importe")]
        public decimal PagoImporte { get; set; }
        [Required(ErrorMessage ="Ingrese la forma de pago")]
        [Display(Name = "Forma de pago")]
        public string PagoForma { get; set; }//Banco o contado
    }
}
