﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace SistemaConsorcio.Models
{
    public class General
    {
        public int GeneralId{get;set;}
        [Display(Name = "Nombre de la administracion")]
        public string GeneralNombreAdministracion { get; set; }
        [Display(Name = "Nombre del consorcio")]
        public string GeneralNombreConsorcio { get; set; }
        [Display(Name = "Nombre de la cuenta bancaria del consorcio")]
        public string GeneralNombreCuentaBancaria { get; set; }
        [Display(Name = "Mail del consorcio")]
        [EmailAddress]
        public string GeneralMailFrom { get; set; }//tiene que ser gmail
        [Display(Name = "Contraseña del mail del consorcio")]
        [DataType(DataType.Password)]
        public string GeneralMailPassword { get; set; }
        [Required(ErrorMessage = "Campo requerido")]
        [Display(Name = "Primer Vencimiento (%)")]
        public decimal GeneralPrimerVencimiento { get; set; }
        [Required(ErrorMessage = "Campo requerido")]
        [Display(Name = "Segundo Vencimiento (%)")]
        public decimal GeneralSegundoVencimiento { get; set; }
        [Required(ErrorMessage = "Campo requerido")]
        [Display(Name = "Tercer Vencimiento (%)")]
        public decimal GeneralTercerVencimiento { get; set; }
        [Required(ErrorMessage = "Campo requerido")]
        [Display(Name = "Valor de cochera")]
        public decimal GeneralPorcentajeCochera { get; set; }
        public int GeneralExpensaActiva { get; set; }
        [Display(Name = "Direccion de la administración")]
        public string GeneralDireccionAdministracion { get; set; }
        [Display(Name = "Telefono administración")]
        public Int64 GeneralTelefonoAdministracion { get; set; }
        [Display(Name = "Cod. area")]
        public int GeneralTelefonoAdministracionCodigo { get; set; }
        [Required(ErrorMessage = "Campo requerido")]
        [Display(Name = "Dia primer vencimiento")]
        public int GeneralDiaPrimerVencimiento { get; set; }
        [Required(ErrorMessage = "Campo requerido")]
        [Display(Name = "Dia segundo vencimiento")]
        public int GeneralDiaSegundoVencimiento { get; set; }
        [Required(ErrorMessage = "Campo requerido")]
        [Display(Name = "Dia tercer vencimiento")]
        public int GeneralDiaTercerVencimiento { get; set; }
        [Display(Name = "Envio de mails automaticos")]
        public bool GeneralEnvioMails { get; set; }
        [Display(Name = "Vencimiento con dias habiles")]
        public bool GeneralVencimientoHabiles { get; set; }

    }
}
