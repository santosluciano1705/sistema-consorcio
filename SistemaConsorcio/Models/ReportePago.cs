﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SistemaConsorcio.Models
{
    public class ReportePago
    {
        public Expensa expensa { get; set; }
        public General general { get; set; }
        public Departamento departamento { get; set; }
        public PagoPendiente pagoPendiente { get; set; }

        public decimal totalExpensa { get; set; }
        public decimal totalCochera { get; set; }
        public decimal totalDepartamento { get; set; }
        public decimal coeficienteCochera { get; set; }
    }
}
