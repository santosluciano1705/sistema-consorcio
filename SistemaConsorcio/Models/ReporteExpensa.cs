﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SistemaConsorcio.Models
{
    public class ReporteExpensa
    {
        public Expensa expensa { get; set; }
        public General general { get; set; }
        public List<Movimientos> movimientos { get; set; }
        public List<Movimientos> movimientosInfo { get; set; }
        public List<RubroExpensa> rubros { get; set; }
        public decimal subTotal { get; set; }
        public decimal fondoReserva { get; set; }
    }
}
