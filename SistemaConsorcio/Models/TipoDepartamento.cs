﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace SistemaConsorcio.Models
{
    public class TipoDepartamento
    {
        public int TipoDepartamentoId { get; set; }
        [Required(ErrorMessage = "Debe ingresar una letra para el departamento")]
        [MaxLength(1, ErrorMessage ="Ingrese una sola letra")]
        [RegularExpression("^[a-zA-Z ]*$", ErrorMessage = "Solo se permite ingresar una letra")]
        [Display(Name = "Letra")]
        public string TipoDepartamentoLetra { get; set; }
        [Required(ErrorMessage = "El departamento tiene que tener un coeficiente")]
        [Display(Name = "Coeficiente (%)")]
        [Range(0, 100.0000, ErrorMessage = "El coeficiente no puede superar el %100")]
        [RegularExpression(@"^\d+.?\d{0,2}$", ErrorMessage = "Invalid Target Price; Maximum Two Decimal Points.")]
        public decimal TipoDepartamentoCoeficiente { get; set; }
    }

}
