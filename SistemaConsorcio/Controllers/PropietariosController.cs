﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Rotativa.AspNetCore;
using Rotativa.AspNetCore.Options;
using SistemaConsorcio.Data;
using SistemaConsorcio.Models;

namespace SistemaConsorcio.Controllers
{
    [Authorize]
    public class PropietariosController : Controller
    {
        private readonly ApplicationDbContext _context;

        public PropietariosController(ApplicationDbContext context)
        {
            _context = context;
        }

        public async Task<IActionResult> getPropietarios()
        {
            var propietarios = _context.Propietario;
            return PartialView("_Index",await propietarios.OrderBy(p => p.PropietarioNombre).ToListAsync());
        }

        public async Task<ViewAsPdf> ReportePropietarios()//si voy a generar byte le pongo async
        {
            GeneralesController mail = new GeneralesController(_context);//controlador del mail
            var pdf = new ViewAsPdf("_ReportePropietarios",_context.Propietario.ToList())
            {
                PageSize = Size.A4
            };
            byte[] bytes = await pdf.BuildFile(this.ControllerContext);//genera pdf para enviar, se podria guardar en una variable
            mail.enviarMailAdjunto(bytes,"luchosan74@gmail.com","prueba","eh guacho","InformePropietarios");//ejemplo llamada para mandar mail
            return pdf;
        }
        // GET: Propietarios/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var propietario = await _context.Propietario
               .FirstOrDefaultAsync(m => m.PropietarioId == id);
            if (propietario == null)
            {
                return NotFound();
            }

            return View(propietario);
        }

        // GET: Propietarios/Create
        public IActionResult Create()
        {
            ViewData["DepartamentoId"] = new SelectList(_context.TipoDepartamento, "TipoDepartamentoId", "TipoDepartamentoLetra");
            return PartialView("_Create");
        }

        [HttpPost]
        public IActionResult Agregar([Bind("PropietarioNombre,PropietarioCuil,PropietarioEmail,PropietarioSuscripto,PropietarioDireccion,PropietarioTelefono")] Propietario propietario)
        {
            if (ModelState.IsValid)
            {
                _context.Add(propietario);
                _context.SaveChanges();
                return Json(new { exitoso = true, alerta = "Agregado correctamente" });
            }
            return Json(new { exitoso = false, alerta = "Error al agregar Departamento" });
        }

        // GET: Propietarios/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var propietario = await _context.Propietario.FindAsync(id);
            if (propietario == null)
            {
                return NotFound();
            }
            return PartialView("_Edit",propietario);
        }

        [HttpPost]
        public async Task<IActionResult> ActualizarPropietario([Bind("PropietarioId,PropietarioNombre,PropietarioCuil,PropietarioEmail,PropietarioSuscripto,PropietarioDireccion,PropietarioTelefono")] Propietario propietario)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(propietario);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!PropietarioExists(propietario.PropietarioId))
                    {
                        return Json(new { exitoso = false, alerta = "Error en la actualizacion" });
                    }
                    else
                    {
                        throw;
                    }
                }
                return Json(new { exitoso = true, alerta = "Actualizado correctamente" });
            }
            return View(propietario);
        }

        // GET: Propietarios/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            var cantDepartamentos = _context.Departamento.Where(d => d.PropietarioId == id).Count();
            if (cantDepartamentos == 0)
            {
                var propietario = await _context.Propietario.FindAsync(id);
                _context.Propietario.Remove(propietario);
                await _context.SaveChangesAsync();
                return Json(new { eliminado = true, msg = "Propietario eliminado" });
            }
            else
            {
                return Json(new { eliminado = false, msg = "No se puede eliminar. Tiene departamentos asociados" });
            }
        }

        private bool PropietarioExists(int id)
        {
            return _context.Propietario.Any(e => e.PropietarioId == id);
        }
    }
}
