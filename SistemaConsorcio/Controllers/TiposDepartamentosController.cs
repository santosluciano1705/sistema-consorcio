﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using SistemaConsorcio.Data;
using SistemaConsorcio.Models;

namespace SistemaConsorcio.Controllers
{
    [Authorize]
    public class TiposDepartamentosController : Controller
    {
        private readonly ApplicationDbContext _context;

        public TiposDepartamentosController(ApplicationDbContext context)
        {
            _context = context;
        }

        // GET: Departamentos
        public async Task<IActionResult> Index()
        {
            return View(await _context.TipoDepartamento.ToListAsync());
        }

        public async Task<IActionResult> getTipos()
        {
            var tipos = _context.TipoDepartamento;
            return PartialView("_index",await tipos.OrderBy(t => t.TipoDepartamentoLetra).ToListAsync());
        }

        // GET: Departamentos/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var departamento = await _context.TipoDepartamento
                .FirstOrDefaultAsync(m => m.TipoDepartamentoId == id);
            if (departamento == null)
            {
                return NotFound();
            }

            return View(departamento);
        }

        // GET: Departamentos/Create
        public IActionResult Create()
        {
            return PartialView("_Create");
        }

        // POST: Departamentos/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("TipoDepartamentoId,TipoDepartamentoLetra,TipoDepartamentoCoeficiente")] TipoDepartamento departamento)
        {
            if (ModelState.IsValid)
            {
                _context.Add(departamento);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(departamento);
        }

        [HttpPost]
        public IActionResult Agregar([Bind("TipoDepartamentoLetra,TipoDepartamentoCoeficiente")] TipoDepartamento departamento)
        {
            if (ModelState.IsValid)
            {
                _context.Add(departamento);
                _context.SaveChanges();
                return Json(new { exitoso = true, alerta = "Agregado correctamente" });
            }
            else
            {
                return Json(new { exitoso = false, alerta = "Error al agregar Departamento" });
            }
        }


        // GET: Departamentos/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var departamento = await _context.TipoDepartamento.FindAsync(id);
            if (departamento == null)
            {
                return NotFound();
            }
            return PartialView("_Edit",departamento);
        }

        // POST: Departamentos/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("TipoDepartamentoId,TipoDepartamentoLetra,TipoDepartamentoCoeficiente")] TipoDepartamento departamento)
        {
            if (id != departamento.TipoDepartamentoId)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(departamento);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!DepartamentoExists(departamento.TipoDepartamentoId))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(departamento);
        }

        [HttpPost]
        public ActionResult ActualizarTipoDepartamento(TipoDepartamento departamento)
        {
            if ((ModelState.IsValid))
            {
                try
                {
                    _context.Update(departamento);
                    if (ValidCoeficiente(departamento))
                    {
                        _context.SaveChanges();
                        return Json(new { exitoso = true, alerta = "Actualizado correctamente" });
                    }
                    else
                    {
                        return Json(new { exitoso = false, alerta = "El coeficiente excede el 100%" });
                    }
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!DepartamentoExists(departamento.TipoDepartamentoId))
                    {
                        return Json(new { exitoso = false, alerta = "Error en la actualizacion" });
                    }
                    else
                    {
                        return Json(new { exitoso = false, alerta = "Error en la actualizacion" });
                    }
                }
            }
            return Json(new { exitoso = true, alerta = "Campos incorrectos" });
        }


        // GET: Departamentos/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            var cantDepartamentos = _context.Departamento.Where(d => d.TipoDepartamentoId == id).Count();
            if (cantDepartamentos == 0)
            {
                var tipodepartamento = await _context.TipoDepartamento.FindAsync(id);
                _context.TipoDepartamento.Remove(tipodepartamento);
                await _context.SaveChangesAsync();
                return Json(new { eliminado = true, msg = "Tipo de Departamento eliminado" });
            }
            else
            {
                return Json(new { eliminado = false, msg = "No se puede eliminar. Tiene departamentos asociados" });
            }
        }

        // POST: Departamentos/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var departamento = await _context.TipoDepartamento.FindAsync(id);
            _context.TipoDepartamento.Remove(departamento);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool DepartamentoExists(int id)
        {
            return _context.TipoDepartamento.Any(e => e.TipoDepartamentoId == id);
        }

        private bool ValidCoeficiente(TipoDepartamento tipoDepartamento)
        {
            var totalCoeficientes = _context.Departamento.Include(d => d.DepartamentoTipo).Where(d => d.TipoDepartamentoId != tipoDepartamento.TipoDepartamentoId).Sum(d => d.DepartamentoTipo.TipoDepartamentoCoeficiente);
            var multiplicidad = _context.Departamento.Include(d => d.DepartamentoTipo).Where(d => d.TipoDepartamentoId == tipoDepartamento.TipoDepartamentoId).Count();
            totalCoeficientes += (multiplicidad * tipoDepartamento.TipoDepartamentoCoeficiente);
            if (totalCoeficientes <= 100)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
    }
}
