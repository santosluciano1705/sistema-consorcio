﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using SistemaConsorcio.Data;
using SistemaConsorcio.Models;

namespace SistemaConsorcio.Controllers
{
    [Authorize]
    public class MovimientosController : Controller
    {
        private readonly ApplicationDbContext _context;

        public MovimientosController(ApplicationDbContext context)
        {
            _context = context;
        }

        // GET: Movimientos
        public async Task<IActionResult> Index()
        {
            var applicationDbContext = _context.Movimientos.Include(m => m.MovimientosExpensa).Include(m => m.MovimientosRubro);
            return View(await applicationDbContext.ToListAsync());
        }
        public async  Task<IActionResult> getMovimientosExpensa(int? id)
        {
            var applicationDbContext = _context.Movimientos
                .Include(m => m.MovimientosExpensa)
                .Include(m => m.MovimientosRubro)
                .Where(m => m.ExpensaId == id)
                .OrderBy(m => m.MovimientosRubro.RubroExpensaDescripcion);
            ViewBag.expensa = id;
            return PartialView("_index", await applicationDbContext.ToListAsync());
        }

        public async Task<IActionResult> getInfoMovimientosExpensa(int? id)
        {
            var applicationDbContext = _context.Movimientos
                .Include(m => m.MovimientosExpensa)
                .Include(m => m.MovimientosRubro)
                .Where(m => m.ExpensaId == id)
                .OrderBy(m => m.MovimientosRubro.RubroExpensaDescripcion); 
            return PartialView("_InfoMovimientos", await applicationDbContext.ToListAsync());
        }

        // GET: Movimientos/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var movimientos = await _context.Movimientos
                .Include(m => m.MovimientosExpensa)
                .Include(m => m.MovimientosRubro)
                .FirstOrDefaultAsync(m => m.MovimientosId == id);
            if (movimientos == null)
            {
                return NotFound();
            }

            return View(movimientos);
        }

        // GET: Movimientos/Create
        public IActionResult Create(int? id)
        {
            ViewData["RubroExpensaId"] = new SelectList(_context.RubroExpensa.OrderBy(m => m.RubroExpensaDescripcion), "RubroExpensaId", "RubroExpensaDescripcion");
            ViewBag.idExpensa = id;
            return PartialView();
        }

        // POST: Movimientos/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("MovimientosId,MovimientosDescripcion,MovimientosPagado,MovimientosImporte,RubroExpensaId,ExpensaId,MovimientosInformacion")] Movimientos movimientos)
        {
            if (ModelState.IsValid)
            {
                _context.Add(movimientos);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            ViewData["ExpensaId"] = new SelectList(_context.Expensa, "ExpensaId", "ExpensaId", movimientos.ExpensaId);
            ViewData["RubroExpensaId"] = new SelectList(_context.RubroExpensa, "RubroExpensaId", "RubroExpensaId", movimientos.RubroExpensaId);
            return View(movimientos);
        }

        [HttpPost]
        public async Task<IActionResult> Agregar([Bind("MovimientosId,MovimientosDescripcion,MovimientosPagado,MovimientosImporte,RubroExpensaId,ExpensaId,MovimientosInformacion")] Movimientos movimientos)
        {
            if (ModelState.IsValid)
            {
                _context.Add(movimientos);
                await _context.SaveChangesAsync();
                return Json(new { exitoso = true, alerta = "Movimiento agregado" });
            }
            return Json(new { exitoso = false, alerta = "Error al agregar" });
        }

        // GET: Movimientos/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var movimientos = await _context.Movimientos.FindAsync(id);
            if (movimientos == null)
            {
                return NotFound();
            }
            ViewBag.idExpensa = id;
            ViewData["RubroExpensaId"] = new SelectList(_context.RubroExpensa, "RubroExpensaId", "RubroExpensaDescripcion");
            return PartialView(movimientos);
        }

        [HttpPost]
        public async Task<IActionResult> ActualizarMovimiento([Bind("MovimientosId,MovimientosDescripcion,MovimientosPagado,MovimientosImporte,RubroExpensaId,ExpensaId,MovimientosInformacion")] Movimientos movimientos)
        {
            if (ModelState.IsValid)
            {
                _context.Update(movimientos);
                await _context.SaveChangesAsync();
                return Json(new { exitoso = true, alerta = "Movimiento actualizado" });
            }
            return Json(new { exitoso = false, alerta = "Error al agregar"});
        }

        // GET: Movimientos/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            var movimientos = await _context.Movimientos.FindAsync(id);
            _context.Movimientos.Remove(movimientos);
            await _context.SaveChangesAsync();
            return Json(new { eliminado = true, msg = "Movimiento eliminado" });
        }

        // POST: Movimientos/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var movimientos = await _context.Movimientos.FindAsync(id);
            _context.Movimientos.Remove(movimientos);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool MovimientosExists(int id)
        {
            return _context.Movimientos.Any(e => e.MovimientosId == id);
        }
    }
}
