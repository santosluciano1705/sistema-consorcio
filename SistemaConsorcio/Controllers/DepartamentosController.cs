﻿using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using SistemaConsorcio.Data;
using SistemaConsorcio.Models;

namespace SistemaConsorcio.Controllers
{
    [Authorize]
    public class DepartamentosController : Controller
    {
        private readonly ApplicationDbContext _context;

        public DepartamentosController(ApplicationDbContext context)
        {
            _context = context;
        }
        //Trae los departamentos
        public async Task<IActionResult> getDepartamentos() {         
            var departamentos = _context.Departamento.Include(d => d.DepartamentoPropietario).Include(d => d.DepartamentoTipo);
            foreach (Departamento departamento in departamentos)
            {
                departamento.DepartamentoSaldo = await calcularSaldo(departamento.DepartamentoId, DateTime.Now);
            }
            return PartialView("_ListaDepartamentos", await departamentos.ToListAsync());
        }
        //Trae un departamento para editar
        public async Task<IActionResult> getDepartamento(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }
            var departamento = await _context.Departamento.FindAsync(id);
            if (departamento == null)
            {
                return NotFound();
            }
            ViewData["PropietarioId"] = new SelectList(_context.Propietario, "PropietarioId", "PropietarioNombre", departamento.PropietarioId);
            ViewData["TipoDepartamentoId"] = new SelectList(_context.TipoDepartamento, "TipoDepartamentoId", "TipoDepartamentoLetra", departamento.TipoDepartamentoId);
            return PartialView("_Edit", departamento);
        }
        //Envia la ultima expensa del departamento
        public async Task<IActionResult> enviarUltimaExpensa(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }
            var departamento = await _context.Departamento.Include(d => d.DepartamentoPropietario).Where(d => d.DepartamentoId == id).FirstAsync();
            var ultimaExpensa = await _context.Expensa.OrderByDescending(e => e.ExpensaPeriodo).FirstAsync();
            byte[] bytes = ultimaExpensa.ExpensaPdf;
            GeneralesController mail = new GeneralesController(_context);//controlador del mail
            mail.enviarMailAdjunto(bytes,
                                    departamento.DepartamentoPropietario.PropietarioEmail,
                                    "Expensa " + ultimaExpensa.ExpensaPeriodo.ToString(string.Format("dd/MM/yyyy")),
                                    "Se adjunta el resumen de la expensa perteneciente al periodo: " + ultimaExpensa.ExpensaPeriodo.ToString(string.Format("dd/MM/yyyy")),
                                    "Resumen Expensa " + ultimaExpensa.ExpensaPeriodo.ToString(string.Format("dd/MM/yyyy")));//ejemplo llamada para mandar mail
            return Json(true);
        }
        //Calcula el saldo total que debe el departamento
        public async Task<decimal> calcularSaldo(int idDepartamento, DateTime fecha)
        {
            var general = await _context.General.FirstOrDefaultAsync();
            var pagosPendientes = await _context.PagoPendiente.Where(p => p.DepartamentoId == idDepartamento).Include(p => p.PagoPendienteExpensa).ToListAsync();
            decimal saldo = 0;
            foreach (PagoPendiente pendiente in pagosPendientes)
            {
                var periodoPago = pendiente.PagoPendienteExpensa.ExpensaPeriodo;
                if (fecha < pendiente.PagoPendienteFechaPrimerVencimiento)//considero q el primer vencimiento es el 5, no vencio
                {
                    saldo += pendiente.PagoPendienteImporte;
                }
                else if (fecha < pendiente.PagoPendienteFechaSegundoVencimiento)//considero q el segundo vencimiento es el 10, aplico primer vencimiento
                {
                    saldo += pendiente.PagoPendientePrimerVencimiento;
                }
                else if (fecha < pendiente.PagoPendienteFechaTercerVencimiento)//considero que el tercer vencimiento es el 10, aplico segundo vencimiento
                {
                    saldo += pendiente.PagoPendienteSegundoVencimiento;
                }
                else //aplico tercer vencimiento
                {
                    saldo += pendiente.PagoPendienteTercerVencimiento;
                }
            }
            return saldo;
        }
        public async Task<IActionResult> Detalles(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }
            var departamento = await _context.Departamento.Include(d => d.DepartamentoTipo).Include(d => d.DepartamentoPropietario).Where(d => d.DepartamentoId == id).FirstAsync();
            if (departamento == null)
            {
                return NotFound();
            }
            ViewBag.saldo = await calcularSaldo(departamento.DepartamentoId,DateTime.Now);
            ViewBag.pendientes = await _context.PagoPendiente.Include(p => p.PagoPendienteExpensa).Where(p => p.DepartamentoId == departamento.DepartamentoId).ToListAsync();
            DateTime actual = DateTime.Now;
            foreach (PagoPendiente pendiente in ViewBag.pendientes)
            {
                if (actual < pendiente.PagoPendienteFechaPrimerVencimiento)
                {
                    pendiente.PagoPendienteImporte = pendiente.PagoPendienteImporte;
                }
                else if (actual < pendiente.PagoPendienteFechaSegundoVencimiento)
                {
                    pendiente.PagoPendienteImporte = pendiente.PagoPendientePrimerVencimiento;
                }
                else if (actual < pendiente.PagoPendienteFechaTercerVencimiento)
                {
                    pendiente.PagoPendienteImporte = pendiente.PagoPendienteSegundoVencimiento;
                }
                else
                {
                    pendiente.PagoPendienteImporte = pendiente.PagoPendienteTercerVencimiento;
                }
            }
            return PartialView("Detalles",departamento);
        }

        // GET: Departamentos/Create
        public IActionResult Create()
        {
            var tipoDeptos =
            _context.TipoDepartamento.Select(t => new
            {
                t.TipoDepartamentoId,
                t.TipoDepartamentoLetra,
                TipoCoeficiente = string.Format("{0} (% {1})", t.TipoDepartamentoLetra, t.TipoDepartamentoCoeficiente)
            })
            .Where(t => !t.TipoDepartamentoLetra.Contains('Z'))
                        .ToList();
            ViewData["PropietarioId"] = new SelectList(_context.Propietario, "PropietarioId", "PropietarioNombre");
            ViewData["TipoDepartamentoId"] = new SelectList(tipoDeptos, "TipoDepartamentoId", "TipoCoeficiente");
            return PartialView("_Create");
        }

        // POST: Departamentos/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        public IActionResult Agregar([Bind("TipoDepartamentoId,DepartamentoPiso,PropietarioId,DepartamentoCochera,DepartamentoCantidadCochera,DepartamentoSufix")] Departamento departamento)
        {
            if (ModelState.IsValid)
            {
                if (!DepartamentoRepeat(departamento))
                {
                    if (ValidCoeficiente(departamento))
                    {
                        _context.Add(departamento);
                        _context.SaveChanges();
                        return Json(new { exitoso = true, alerta = "Agregado correctamente" });
                    }
                    else
                    {
                        return Json(new { exitoso = false, alerta = "El departamento excede el 100%" });
                    }
                }
                else
                {
                    return Json(new { exitoso = false, alerta = "Departamento repetido" });
                }
            }
            else
            {
                return Json(new { exitoso = false, alerta = "Error al agregar Departamento" });
            }
        }

        [HttpPost]
        public ActionResult ActualizarDepartamento(Departamento departamento)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    Departamento updateDepartamento = _context.Departamento.Find(departamento.DepartamentoId);
                    updateDepartamento.DepartamentoPiso = departamento.DepartamentoPiso; 
                    updateDepartamento.DepartamentoCochera = departamento.DepartamentoCochera; 
                    updateDepartamento.PropietarioId = departamento.PropietarioId; 
                    updateDepartamento.TipoDepartamentoId = departamento.TipoDepartamentoId;
                    updateDepartamento.DepartamentoCantidadCochera = departamento.DepartamentoCantidadCochera;
                    updateDepartamento.DepartamentoSufix = departamento.DepartamentoSufix;
                    if (!DepartamentoRepeat(updateDepartamento))
                    {
                        _context.SaveChanges();
                        return Json(new { exitoso = true, alerta = "Actualizado correctamente" });
                    }
                    else
                    {
                        return Json(new { exitoso = false, alerta = "Departamento repetido" });
                    }
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!DepartamentoExists(departamento.DepartamentoId))
                    {
                        return Json(new { exitoso = false, alerta = "Error en la actualizacion" });
                    }
                    else
                    {
                        return Json(new { exitoso = false, alerta = "Error en la actualizacion" });
                    }
                }
            }
            return Json(new { exitoso = false, alerta = "Campos incorrectos" });
        }

        // GET: Departamentos/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            var departamento = await _context.Departamento.FindAsync(id);
            _context.Departamento.Remove(departamento);
            await _context.SaveChangesAsync();
            return Json(new { eliminado = true, msg = "Departamento eliminado" });
        }
        private bool DepartamentoExists(int id)
        {
            return _context.Departamento.Any(e => e.DepartamentoId == id);
        }

        private bool DepartamentoRepeat(Departamento departamento)
        {
            var departamentos = from d in _context.Departamento
                                where (d.DepartamentoPiso == departamento.DepartamentoPiso)
                                && (departamento.TipoDepartamentoId == d.TipoDepartamentoId)
                                   select d;
            if (departamentos.Contains(departamento)){
                return false;
            }
            else
            {
                return (departamentos.Count() > 0);
            }
        }
        private bool ValidCoeficiente(Departamento departamento)
        {
            var totalCoeficientes = _context.Departamento.Include(d => d.DepartamentoTipo).Sum(d => d.DepartamentoTipo.TipoDepartamentoCoeficiente);
            var nuevoDeptoCoef = _context.TipoDepartamento.Where(t => departamento.TipoDepartamentoId == t.TipoDepartamentoId).First();
            totalCoeficientes += nuevoDeptoCoef.TipoDepartamentoCoeficiente;
            if (totalCoeficientes <= 100)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
    }
}
