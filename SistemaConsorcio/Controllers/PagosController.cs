﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using SistemaConsorcio.Data;
using SistemaConsorcio.Models;

namespace SistemaConsorcio.Controllers
{
    [Authorize]
    public class PagosController : Controller
    {
        private readonly ApplicationDbContext _context;

        public PagosController(ApplicationDbContext context)
        {
            _context = context;
        }

        // GET: Pagos
        public IActionResult Index()
        {
            return PartialView();
        }

        public async Task<IActionResult> getPagos(DateTime? fechaDesde,DateTime? fechaHasta)
        {
            var pagos = from p in _context.Pago
                           select p;
            pagos = _context.Pago.Include(p => p.PagoDepartamento).Include(p => p.PagoDepartamento.DepartamentoTipo).OrderByDescending(p => p.PagoFecha);
            DateTime date = DateTime.Now;
            DateTime oPrimerDiaDelMes = new DateTime(date.Year, date.Month, 1);
            if (!String.IsNullOrEmpty(fechaDesde.ToString()))
            {
                pagos = pagos.Where(p => p.PagoFecha >= fechaDesde);
            }
            else
            {
                pagos = pagos.Where(p => p.PagoFecha >= oPrimerDiaDelMes);
            }
            if (!String.IsNullOrEmpty(fechaHasta.ToString()))
            {
                pagos = pagos.Where(p => p.PagoFecha <= fechaHasta);
            }
            else
            {
                DateTime oUltimoDiaDelMes = oPrimerDiaDelMes.AddMonths(1).AddDays(-1);
                pagos = pagos.Where(p => p.PagoFecha <= oUltimoDiaDelMes);
            }
            ViewBag.totalBanco = pagos.Where(p => p.PagoForma == "Cuenta bancaria").Sum(p => p.PagoImporte);
            ViewBag.totalCaja = pagos.Where(p => p.PagoForma == "Contado").Sum(p => p.PagoImporte);
            ViewBag.total = ViewBag.totalBanco + ViewBag.totalCaja;
            return PartialView("_Listado",await pagos.ToListAsync());
        }
        // GET: Pagos/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var pago = await _context.Pago
                .Include(p => p.PagoDepartamento)
                .FirstOrDefaultAsync(m => m.PagoId == id);
            if (pago == null)
            {
                return NotFound();
            }

            return View(pago);
        }

        // GET: Pagos/Create
        public IActionResult Create()
        {
            var departamentos =
                      _context.Departamento.Include(d => d.DepartamentoTipo)
                        .Select(d => new
                        {
                            d.DepartamentoId,
                            NombreDeparamento = string.Format("{0}°{1}", d.DepartamentoPiso, d.DepartamentoTipo.TipoDepartamentoLetra)
                        })
                        .ToList();
            ViewData["DepartamentoId"] = new SelectList(departamentos, "DepartamentoId", "NombreDeparamento");
            ViewData["ExpensaId"] = new SelectList(_context.Expensa, "ExpensaId", "ExpensaId");
            return PartialView();
        }

        // POST: Pagos/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        public async Task<IActionResult> Create([Bind("PagoId,PagoFecha,DepartamentoId,PagoImporte,PagoForma")] Pago pago, int TipoPago)
        {
            if (ModelState.IsValid)
            {
                if (TipoPago == 0)
                {
                    await eliminarPagosPendientesDepartamento(pago.DepartamentoId);
                }
                _context.Add(pago);
                await _context.SaveChangesAsync();
                return Json(new { exitoso = true, alerta = "Pago cargado" });
            }
            return Json(new { exitoso = false, alerta = "Error al cargar pago" });
        }

        public async Task<IActionResult> eliminarPagosPendientesDepartamento(int idDepartamento)
        {
            var pendientes = await _context.PagoPendiente.Where(pp => pp.DepartamentoId == idDepartamento).ToListAsync();
            var controladorPendientes = new PagoPendientesController(_context);
            foreach (PagoPendiente pendiente in pendientes)
            {
                await controladorPendientes.Delete(pendiente.PagoPendienteId);   
            }
            return Json(true);
        }
        // GET: Pagos/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var pago = await _context.Pago.FindAsync(id);
            if (pago == null)
            {
                return NotFound();
            }
            ViewData["DepartamentoId"] = new SelectList(_context.Departamento, "DepartamentoId", "DepartamentoId", pago.DepartamentoId);
            return View(pago);
        }

        // POST: Pagos/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("PagoId,PagoFecha,DepartamentoId,PagoImporte,PagoForma")] Pago pago)
        {
            if (id != pago.PagoId)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(pago);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!PagoExists(pago.PagoId))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            ViewData["DepartamentoId"] = new SelectList(_context.Departamento, "DepartamentoId", "DepartamentoId", pago.DepartamentoId);
            return View(pago);
        }

        // GET: Pagos/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var pago = await _context.Pago
                .Include(p => p.PagoDepartamento)
                .FirstOrDefaultAsync(m => m.PagoId == id);
            if (pago == null)
            {
                return NotFound();
            }

            return View(pago);
        }

        // POST: Pagos/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var pago = await _context.Pago.FindAsync(id);
            _context.Pago.Remove(pago);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool PagoExists(int id)
        {
            return _context.Pago.Any(e => e.PagoId == id);
        }
    }
}
