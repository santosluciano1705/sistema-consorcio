﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Net;
using System.Net.Mail;
using Microsoft.AspNetCore.Authorization;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Rotativa.AspNetCore;
using Rotativa.AspNetCore.Options;
using SistemaConsorcio.Data;
using SistemaConsorcio.Models;

namespace SistemaConsorcio.Controllers
{
    [Authorize]
    public class PagoPendientesController : Controller
    {
        private readonly ApplicationDbContext _context;

        public PagoPendientesController(ApplicationDbContext context)
        {
            _context = context;
        }

        // GET: PagoPendientes
        public async Task<IActionResult> Index()
        {
            var applicationDbContext = _context.PagoPendiente.Include(p => p.PagoPendienteDepartamento).Include(p => p.PagoPendienteExpensa).Include(t => t.PagoPendienteDepartamento.DepartamentoTipo);
            return PartialView(await applicationDbContext.ToListAsync());
        }
        // POST: PagoPendientes/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("PagoPendienteId,ExpensaId,DepartamentoId,PagoPendienteImporte,PagoPendienteBono")] PagoPendiente pagoPendiente)
        {
            if (ModelState.IsValid)
            {
                await _context.AddAsync(pagoPendiente);
                await _context.SaveChangesAsync();
            }
            ViewData["DepartamentoId"] = new SelectList(_context.Departamento, "DepartamentoId", "DepartamentoId", pagoPendiente.DepartamentoId);
            ViewData["ExpensaId"] = new SelectList(_context.Expensa, "ExpensaId", "ExpensaId", pagoPendiente.ExpensaId);
            return View(pagoPendiente);
        }
        
        //Envia mails a todos los pagos pendientes de los propietarios suscriptos
        public async Task<ActionResult> SendAllMails()
        {
            GeneralesController mail = new GeneralesController(_context);//controlador del mail
            var pagosPendientes = await _context.PagoPendiente
                    .Include(p => p.PagoPendienteDepartamento.DepartamentoPropietario)
                    .Include(p => p.PagoPendienteExpensa)
                    .ToListAsync();
            foreach (PagoPendiente pagoPendiente in pagosPendientes)
            {
                if (pagoPendiente.PagoPendienteDepartamento.DepartamentoPropietario.PropietarioSuscripto)
                {
                    byte[] bytes = pagoPendiente.PagoPendienteBono;
                    mail.enviarMailAdjunto(bytes, pagoPendiente.PagoPendienteDepartamento.DepartamentoPropietario.PropietarioEmail, "Expensa de prueba", "Esta es la expensa perteneciente al periodo 31/05/2019", "Expensa-31-05-19");//ejemplo llamada para mandar mail
                }
            }
            //genera pdf para enviar, se podria guardar en una variable
            return Json(true);
        }

        public async Task<ActionResult> getBonoPago(int? id)
        {
            var pago = await _context.PagoPendiente.FindAsync(id);
            byte[] bytePdf = pago.PagoPendienteBono;
            var name = "Bono pago"+".pdf";
            return Json(new { pdf = bytePdf, pdfName = name });
        }
        // GET: PagoPendientes/Delete/5
        [HttpPost, ActionName("Delete")]
        public async Task<IActionResult> Delete(int? id)
        {
            var pagoPendiente = await _context.PagoPendiente.FindAsync(id);
            _context.PagoPendiente.Remove(pagoPendiente);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool PagoPendienteExists(int id)
        {
            return _context.PagoPendiente.Any(e => e.PagoPendienteId == id);
        }

        public async Task<ActionResult> pagarBono(int id, string formaPago, DateTime fechaPago, int completo, decimal importe)
        {
            var pagoPendiente = await _context.PagoPendiente.Include(pp => pp.PagoPendienteExpensa).Where(pp => pp.PagoPendienteId == id).FirstOrDefaultAsync();
            PagosController pagoController = new PagosController(_context);
            var pago = new Pago
            {
                DepartamentoId = pagoPendiente.DepartamentoId,
                PagoFecha = fechaPago,
                PagoForma = formaPago
            };
            var pagoTotal = await calcularPagoPendiente(id, pago.PagoFecha);
            if (completo == 1 || importe==pagoTotal)
            {
                pago.PagoImporte = pagoTotal;
                await pagoController.Create(pago, 1);//pongo 1 porque es un pago parcial, solo este bono
                await Delete(pagoPendiente.PagoPendienteId);
                return Json(new { success = true, msg = "Pago realizado", liquidado=true });
            }
            else if (importe < pagoTotal)
            {
                pago.PagoImporte = importe;
                await pagoController.Create(pago, 1);//pongo 1 porque es un pago parcial, solo este bono
                _context.Update(realizarPagoParcial(pagoPendiente, importe));
                await _context.SaveChangesAsync();
                return Json(new { success = true, msg = "Pago realizado", liquidado=false });
            }
            else
            {
                return Json(new { success = false, msg = "Pago mayor al saldo", liquidado = false });
            }
        }

        public async Task<decimal> calcularPagoPendiente(int id, DateTime PagoFecha)
        {
            var pagoPendiente = await _context.PagoPendiente.Include(pp => pp.PagoPendienteExpensa).Where(pp => pp.PagoPendienteId == id).FirstOrDefaultAsync();
            if (PagoFecha < pagoPendiente.PagoPendienteFechaPrimerVencimiento)//considero q el primer vencimiento es el 5, no vencio
            {
                return pagoPendiente.PagoPendienteImporte;
            }
            else if (PagoFecha < pagoPendiente.PagoPendienteFechaSegundoVencimiento)//considero q el segundo vencimiento es el 10, aplico primer vencimiento
            {
                return pagoPendiente.PagoPendientePrimerVencimiento;
            }
            else if (PagoFecha < pagoPendiente.PagoPendienteFechaTercerVencimiento)//considero que el tercer vencimiento es el 10, aplico segundo vencimiento
            {
                return pagoPendiente.PagoPendienteSegundoVencimiento;
            }
            else //aplico tercer vencimiento
            {
                return pagoPendiente.PagoPendienteTercerVencimiento;
            }
        }

        private PagoPendiente realizarPagoParcial(PagoPendiente pagoPendiente, decimal importe)
        {
            pagoPendiente.PagoPendienteImporte = (pagoPendiente.PagoPendienteImporte >= importe) ? (pagoPendiente.PagoPendienteImporte - importe) : 0;
            pagoPendiente.PagoPendientePrimerVencimiento = (pagoPendiente.PagoPendientePrimerVencimiento >= importe) ? (pagoPendiente.PagoPendientePrimerVencimiento - importe) : 0;
            pagoPendiente.PagoPendienteSegundoVencimiento = (pagoPendiente.PagoPendienteSegundoVencimiento >= importe) ? (pagoPendiente.PagoPendienteSegundoVencimiento - importe) : 0;
            pagoPendiente.PagoPendienteTercerVencimiento = (pagoPendiente.PagoPendienteTercerVencimiento >= importe) ? (pagoPendiente.PagoPendienteTercerVencimiento - importe) : 0;
            return pagoPendiente;
        }
    }
}
