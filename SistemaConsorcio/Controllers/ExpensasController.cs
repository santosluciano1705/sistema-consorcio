﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Rotativa.AspNetCore;
using Rotativa.AspNetCore.Options;
using SistemaConsorcio.Data;
using SistemaConsorcio.Models;

namespace SistemaConsorcio.Controllers
{
    [Authorize]
    public class ExpensasController : Controller
    {

        private readonly ApplicationDbContext _context;

        public ExpensasController(ApplicationDbContext context)
        {
            _context = context;
        }

        public async Task<IActionResult> getExpensas()
        {
            return PartialView("_Index",await _context.Expensa.OrderByDescending(e => e.ExpensaPeriodo).ToListAsync());
        }

        public async Task<IActionResult> getExpensaActual()
        {
            var configuracion = await _context.General.FirstOrDefaultAsync();
            var id = configuracion.GeneralExpensaActiva;
            var expensa = await _context.Expensa.FindAsync(id);
            return PartialView("_Carga",expensa);
        }

        // GET: Expensas/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }
            var expensa = await _context.Expensa
                .FirstOrDefaultAsync(m => m.ExpensaId == id);
            if (expensa == null)
            {
                return NotFound();
            }
            ViewBag.subTotal = await calcularTotalExpensa(expensa) - expensa.ExpensaReserva;
            return PartialView(expensa);
        }

        [HttpPost]
        public async Task<IActionResult> Create([Bind("ExpensaId,ExpensaPeriodo,ExpensaConfirmada")] Expensa expensa)
        {
            DateTime date = DateTime.Now;
            DateTime oPrimerDiaDelMes = new DateTime(date.Year, date.Month, 1);
            DateTime oUltimoDiaDelMes = oPrimerDiaDelMes.AddMonths(1).AddDays(-1);
            expensa.ExpensaPeriodo = oUltimoDiaDelMes;
            var expensas = from e in _context.Expensa
                                where (e.ExpensaPeriodo.Month == expensa.ExpensaPeriodo.Month) && ( e.ExpensaPeriodo.Year == expensa.ExpensaPeriodo.Year)
                                select e;
            if (expensas.Count() > 0)
            {
                return Json(new { exitoso = false, alerta = "Ya hay una expensa cargada para el mes" });
            }
            expensa.ExpensaReserva = 0;
            _context.Add(expensa);
            await _context.SaveChangesAsync();
            var configuracion = await _context.General.FirstOrDefaultAsync();
            configuracion.GeneralExpensaActiva = expensa.ExpensaId;
            await _context.SaveChangesAsync();
            return Json(new { exitoso = true, alerta = "Agregado correctamente",id = expensa.ExpensaId });
        }

        public async Task<IActionResult> ConfirmarExpensa(int id)
        {
            var expensa = await _context.Expensa.FindAsync(id);
            var general = await _context.General.FirstOrDefaultAsync();
            await generarPagos(expensa);
            expensa.ExpensaConfirmada = true;
            general.GeneralExpensaActiva = 0;
            _context.Update(expensa);
            _context.Update(general);
            if (general.GeneralEnvioMails)
            {
                await SendAllMailsExpensa(expensa);
            }
            await _context.SaveChangesAsync();
            return Json(true);
        }

        private async Task<IActionResult> generarPagos(Expensa expensa)
        {
            var departamentos = await _context.Departamento.ToListAsync();
            var crearPagoPendiente = new PagoPendientesController(_context);
            var totalExpensa = await calcularTotalExpensa(expensa);
            var totalCochera = await calcularTotalCochera(totalExpensa);
            var cantidadCocheras = departamentos.Sum(d => d.DepartamentoCantidadCochera);
            var totalDepartamento = totalExpensa - (totalCochera*cantidadCocheras);
            PagoPendiente fechas = await calcularFechaVencimientos(expensa.ExpensaPeriodo);
            foreach (Departamento departamento in departamentos)
            {
                await crearPagoPendiente.Create(await generarPagoPendiente(expensa, departamento, totalDepartamento, fechas, totalCochera));
            }
            return Json(true);
        }

        private async Task<PagoPendiente> calcularFechaVencimientos(DateTime periodoExpensa)
        {
            var configuracion = await _context.General.FirstAsync();
            periodoExpensa = periodoExpensa.AddDays(1);
            var conteo = 1;
            PagoPendiente aux = new PagoPendiente();
            while (conteo <= configuracion.GeneralDiaTercerVencimiento)
            {
                if (periodoExpensa.DayOfWeek == DayOfWeek.Saturday && configuracion.GeneralVencimientoHabiles)
                {
                    periodoExpensa = periodoExpensa.AddDays(2);
                }
                if (periodoExpensa.DayOfWeek == DayOfWeek.Sunday && configuracion.GeneralVencimientoHabiles)
                {
                    periodoExpensa = periodoExpensa.AddDays(1);
                }
                if (configuracion.GeneralDiaPrimerVencimiento == conteo)
                {
                    aux.PagoPendienteFechaPrimerVencimiento = periodoExpensa;
                    periodoExpensa = periodoExpensa.AddDays(1);
                    conteo++;
                }
                else if (configuracion.GeneralDiaSegundoVencimiento == conteo)
                {
                    aux.PagoPendienteFechaSegundoVencimiento = periodoExpensa;
                    periodoExpensa = periodoExpensa.AddDays(1);
                    conteo++;
                }
                else if (configuracion.GeneralDiaTercerVencimiento == conteo)
                {
                    aux.PagoPendienteFechaTercerVencimiento = periodoExpensa;
                    periodoExpensa = periodoExpensa.AddDays(1);
                    conteo++;
                }
                else
                {
                    periodoExpensa = periodoExpensa.AddDays(1);
                    conteo++;
                }
            }
            return aux;
        }
        private async Task<PagoPendiente> generarPagoPendiente(Expensa expensa, Departamento departamento, decimal totalExpensa, PagoPendiente fechas, decimal totalCochera)
        {
            var tipo = new TipoDepartamento();
            decimal importe = 0;
            var descripcion = "Departamento";
            if (departamento.DepartamentoCochera)
            {
                tipo = await _context.TipoDepartamento
                        .FirstOrDefaultAsync(t => t.TipoDepartamentoLetra == "Z");
                importe += totalCochera*departamento.DepartamentoCantidadCochera;
                descripcion += "/Cochera";
            }
            tipo = await _context.TipoDepartamento
                      .FirstOrDefaultAsync(t => t.TipoDepartamentoId == departamento.TipoDepartamentoId);
            importe += (((totalExpensa) * tipo.TipoDepartamentoCoeficiente) / 100);
            importe = roundImport(importe,departamento);
            var nuevoPagoPendiente = new PagoPendiente
            {
                ExpensaId = expensa.ExpensaId,
                DepartamentoId = departamento.DepartamentoId,
                PagoPendienteImporte = importe,
                PagoPendienteFechaPrimerVencimiento = fechas.PagoPendienteFechaPrimerVencimiento,
                PagoPendienteFechaSegundoVencimiento = fechas.PagoPendienteFechaSegundoVencimiento,
                PagoPendienteFechaTercerVencimiento = fechas.PagoPendienteFechaTercerVencimiento,
                PagoPendienteDescripcion = descripcion
            };
            var general = await _context.General.FirstOrDefaultAsync();
            nuevoPagoPendiente.PagoPendientePrimerVencimiento = roundImport(Math.Floor(nuevoPagoPendiente.PagoPendienteImporte + ((nuevoPagoPendiente.PagoPendienteImporte * general.GeneralPrimerVencimiento) / 100)),departamento);
            nuevoPagoPendiente.PagoPendienteSegundoVencimiento = roundImport(Math.Floor(nuevoPagoPendiente.PagoPendienteImporte + ((nuevoPagoPendiente.PagoPendienteImporte * general.GeneralSegundoVencimiento) / 100)),departamento);
            nuevoPagoPendiente.PagoPendienteTercerVencimiento = roundImport(Math.Floor(nuevoPagoPendiente.PagoPendienteImporte + ((nuevoPagoPendiente.PagoPendienteImporte * general.GeneralTercerVencimiento) / 100)),departamento);
            nuevoPagoPendiente.PagoPendienteBono = await crearBonoPago(nuevoPagoPendiente);
            return nuevoPagoPendiente;
        }

        private decimal roundImport(decimal importe, Departamento departamento)
        {
            string sufixChar = null;
            if (departamento.DepartamentoSufix.ToString().Length == 1)
            {
                sufixChar = "0.0" + departamento.DepartamentoSufix;
            }
            else
            {
                sufixChar = "0." + departamento.DepartamentoSufix;
            }
            decimal sufix = Convert.ToDecimal(sufixChar);
            importe = Math.Floor(importe);
            return importe+sufix;
        }
        public async Task<byte[]> crearBonoPago(PagoPendiente pago)
        {
            ReportePago datos = new ReportePago();
            datos.general = await _context.General.FirstOrDefaultAsync();
            datos.expensa = await _context.Expensa.Where(e => e.ExpensaId == pago.ExpensaId).FirstAsync();
            datos.departamento = await _context.Departamento.Include(d => d.DepartamentoPropietario).Include(d => d.DepartamentoTipo).Where(d => d.DepartamentoId == pago.DepartamentoId).FirstAsync();
            datos.pagoPendiente = pago;
            datos.totalExpensa = await calcularTotalExpensa(datos.expensa);
            var departamentos = await _context.Departamento.Include(d => d.DepartamentoTipo).ToListAsync();
            var cantidadDepartamentos = departamentos.Sum(d => d.DepartamentoCantidadCochera);
            datos.totalCochera = await calcularTotalCochera(datos.totalExpensa);
            datos.totalDepartamento = datos.totalExpensa - (datos.totalCochera*cantidadDepartamentos);
            var pdf = new ViewAsPdf("BonoPago", datos)
            {
                FileName = "bonoPago.pdf",
                PageSize = Size.A4
            };
            byte[] bono = await pdf.BuildFile(this.ControllerContext);//genera pdf para enviar, se podria guardar en una variable
            return bono;
        }

        public async Task<ActionResult> generarBonoPago(int pagoPendienteId)
        {
            var pagoPendiente = await _context.PagoPendiente.FindAsync(pagoPendienteId);
            byte[] bytePdf = await crearBonoPago(pagoPendiente);            
            var name = "Bono de Pago - " + pagoPendiente.PagoPendienteId + ".pdf";
            pagoPendiente.PagoPendienteBono = bytePdf;
            _context.Update(pagoPendiente);
            return Json(new { pdf = bytePdf, pdfName = name });
        }

        public async Task<ActionResult> enviarBonoPago(int pagoPendienteId, string mailPropietario)
        {
            var pagoPendiente = await _context.PagoPendiente
                .Include(p => p.PagoPendienteDepartamento.DepartamentoPropietario)
                .Include(p => p.PagoPendienteExpensa)
                .Where(p => p.PagoPendienteId == pagoPendienteId)
                .FirstAsync();
            GeneralesController mail = new GeneralesController(_context);//controlador del mail
            mail.enviarMailAdjunto(pagoPendiente.PagoPendienteBono, mailPropietario,
                    "Bono de pago -  " + pagoPendiente.PagoPendienteExpensa.ExpensaPeriodo.ToString(string.Format("dd/MM/yyyy")) + " - " + pagoPendiente.PagoPendienteDescripcion,
                    "Se adjunto el bono de pago (" + pagoPendiente.PagoPendienteDescripcion + ") perteneciente al periodo: " + pagoPendiente.PagoPendienteExpensa.ExpensaPeriodo.ToString(string.Format("dd/MM/yyyy")),
                    "Pago-" + pagoPendiente.PagoPendienteExpensa.ExpensaPeriodo.ToString(string.Format("dd/MM/yyyy")) + "(" + pagoPendiente.PagoPendienteDescripcion + ")");
            return Json(new { success = true});
        }

        public async Task<ActionResult> LiquidarExpensa(int id, decimal expensaReserva)
        {
            var expensa = await _context.Expensa.FindAsync(id);
            expensa.ExpensaReserva = expensaReserva;
            byte[] bytePdf = await GenerarReporteExpensa(expensa);
            byte[] listPdf = await GenerarCalculoDepartamentos(expensa);
            var name = "Expensa " + expensa.ExpensaPeriodo.ToString(string.Format("dd/MM/yyyy")) + ".pdf";
            var nameList = "Calculo Departamentos - "+expensa.ExpensaPeriodo.ToString(string.Format("dd/MM/yyyy")) + ".pdf";
            return Json(new { success = true, pdf = bytePdf, pdfName = name, reserva = expensa.ExpensaReserva, total = await calcularTotalExpensa(expensa), pdfList = listPdf, pdfListName = nameList });
        }

        public async Task<ActionResult> getResumen(int? id)
        {
            var expensa = await _context.Expensa.FindAsync(id);
            byte[] bytePdf = expensa.ExpensaPdf;
            var name = "Expensa " + expensa.ExpensaPeriodo.ToString(string.Format("dd/MM/yyyy")) + ".pdf";
            return Json(new { pdf = bytePdf, pdfName = name });
        }


        private async Task<decimal> calcularTotalCochera(decimal total)
        {
            General gen = await _context.General.FirstOrDefaultAsync();
            return gen.GeneralPorcentajeCochera;            
        }
        private async Task<decimal> calcularTotalPagos(DateTime fecha)
        {
            DateTime oPrimerDiaDelMes = new DateTime(fecha.Year, fecha.Month, 1);
            DateTime oUltimoDiaDelMes = oPrimerDiaDelMes.AddMonths(1).AddDays(-1);
            var pagos = await _context.Pago.Where(p => p.PagoFecha >= oPrimerDiaDelMes && p.PagoFecha <= oUltimoDiaDelMes).ToListAsync();
            decimal total = 0;
            foreach (Pago pago in pagos)
            {
                total += pago.PagoImporte;
            }
            return total;
        }

        public async Task<byte[]> GenerarReporteExpensa(Expensa expensa)
        {
            ReporteExpensa db = new ReporteExpensa();
            db.subTotal = await calcularTotalExpensa(expensa) - expensa.ExpensaReserva; 
            db.expensa = expensa;
            db.general = await _context.General.FirstOrDefaultAsync();
            db.movimientos = await _context.Movimientos
                                            .Where(m => m.ExpensaId.Equals(expensa.ExpensaId) && m.MovimientosInformacion.Equals(false))
                                            .ToListAsync();
            db.movimientosInfo = await _context.Movimientos
                                            .Where(m => m.ExpensaId.Equals(expensa.ExpensaId) && m.MovimientosInformacion.Equals(true))
                                            .ToListAsync();
            DateTime date = DateTime.Now;
            DateTime oPrimerDiaDelMes = new DateTime(date.Year, date.Month, 1);
            DateTime oUltimoDiaDelMes = oPrimerDiaDelMes.AddMonths(1).AddDays(-1);
            Movimientos nuevoMovimiento = new Movimientos();
            nuevoMovimiento.MovimientosDescripcion = "Pagos de expensas ("+ oPrimerDiaDelMes.ToString(string.Format("dd/MM/yyyy")) + "-" + oUltimoDiaDelMes.ToString(string.Format("dd/MM/yyyy")) + ")";
            nuevoMovimiento.ExpensaId = expensa.ExpensaId;
            nuevoMovimiento.MovimientosImporte = await calcularTotalPagos(expensa.ExpensaPeriodo);
            nuevoMovimiento.MovimientosInformacion = true;
            nuevoMovimiento.MovimientosPagado = true;
            db.movimientosInfo.Add(nuevoMovimiento);
            db.rubros = await _context.RubroExpensa
                                        .Where(r => db.movimientos.Where(m => r.RubroExpensaId==m.RubroExpensaId).Count()>0)
                                        .ToListAsync();
            var pdf = new ViewAsPdf("ResumenExpensa", db)
            {
                FileName = "expensa.pdf",
                PageSize = Size.A4
            };
            byte[] reporte = await pdf.BuildFile(this.ControllerContext);//genera pdf para enviar, se podria guardar en una variable
            expensa.ExpensaPdf = reporte;
            _context.Update(expensa);
            await _context.SaveChangesAsync();
            return reporte;
        }

        public async Task<byte[]> GenerarCalculoDepartamentos(Expensa expensa)
        {
            ReporteCalculoDepartamentos db = new ReporteCalculoDepartamentos();
            db.total = await calcularTotalExpensa(expensa);
            db.departamentos = await _context.Departamento.Include(d => d.DepartamentoTipo).OrderBy(d => d.DepartamentoPiso).OrderBy(d => d.DepartamentoTipo.TipoDepartamentoLetra).ToListAsync();
            db.cantidadCocheras = db.departamentos.Sum(d => d.DepartamentoCantidadCochera);
            db.general = await _context.General.FirstOrDefaultAsync();
            db.total = db.total - (db.cantidadCocheras * db.general.GeneralPorcentajeCochera);
            db.expensa = expensa;
            var pdf = new ViewAsPdf("ListadoLiquidacionDepto", db)
            {
                FileName = "calculoDepartamentos.pdf",
                PageSize = Size.A4
            };
            byte[] reporte = await pdf.BuildFile(this.ControllerContext);//genera pdf para enviar, se podria guardar en una variable
            return reporte;
        }

        public async Task<decimal> calcularTotalExpensa(Expensa expensa)
        {
            var movimientos = await _context.Movimientos
                                .Where(m => m.ExpensaId == expensa.ExpensaId && m.MovimientosInformacion == false)
                                .ToListAsync();
            decimal total = 0;
            foreach (Movimientos movimiento in movimientos)
            {

                total += movimiento.MovimientosImporte;
            }
            total += expensa.ExpensaReserva;
            return total;
        }

        public async Task<ActionResult> SendAllMailsExpensa(Expensa expensa)
        {
            GeneralesController mail = new GeneralesController(_context);//controlador del mail
            var pagosPendientes = await _context.PagoPendiente
                    .Include(p => p.PagoPendienteDepartamento.DepartamentoPropietario)
                    .Include(p => p.PagoPendienteExpensa)
                    .Where(p => p.ExpensaId == expensa.ExpensaId)
                    .ToListAsync();
            foreach (PagoPendiente pagoPendiente in pagosPendientes)
            {
                if (pagoPendiente.PagoPendienteDepartamento.DepartamentoPropietario.PropietarioSuscripto)
                {
                    byte[] bytes = pagoPendiente.PagoPendienteBono;
                    mail.enviarMailAdjunto(bytes, 
                        pagoPendiente.PagoPendienteDepartamento.DepartamentoPropietario.PropietarioEmail,  
                        "Bono de pago -  " + pagoPendiente.PagoPendienteExpensa.ExpensaPeriodo.ToString(string.Format("dd/MM/yyyy")) + " - "+ pagoPendiente.PagoPendienteDescripcion, 
                        "Se adjunto el bono de pago ("+ pagoPendiente.PagoPendienteDescripcion+") perteneciente al periodo: " + pagoPendiente.PagoPendienteExpensa.ExpensaPeriodo.ToString(string.Format("dd/MM/yyyy")), 
                        "Pago-"+pagoPendiente.PagoPendienteExpensa.ExpensaPeriodo.ToString(string.Format("dd/MM/yyyy")) + "("+pagoPendiente.PagoPendienteDescripcion+")");//ejemplo llamada para mandar mail
                }
            }
            var propietarios = await _context.Propietario.ToListAsync();
            foreach (Propietario propietario in propietarios)
            {
                if (propietario.PropietarioSuscripto)
                {
                    byte[] bytes = expensa.ExpensaPdf;
                    mail.enviarMailAdjunto(bytes, 
                                            propietario.PropietarioEmail,
                                            "Expensa "+expensa.ExpensaPeriodo.ToString(string.Format("dd/MM/yyyy")),
                                            "Se adjunta el resumen de la expensa perteneciente al periodo: " + expensa.ExpensaPeriodo.ToString(string.Format("dd/MM/yyyy")),
                                            "Resumen-Expensa-"+expensa.ExpensaPeriodo.ToString(string.Format("dd/MM/yyyy")));//ejemplo llamada para mandar mail
                }
            }
            //genera pdf para enviar, se podria guardar en una variable
            return Json(true);
        }

        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var expensa = await _context.Expensa.FindAsync(id);
            if (expensa == null)
            {
                return NotFound();
            }
            return PartialView(expensa);
        }

        [HttpPost]
        public ActionResult ActualizarExpensa(Expensa expensa)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(expensa);
                    _context.SaveChanges();
                    return Json(new { exitoso = true, alerta = "Actualizado correctamente" });
                }
                catch (DbUpdateConcurrencyException)
                {
                    return Json(new { exitoso = false, alerta = "Error en la actualizacion" });
                }
            }
            return Json(new { exitoso = false, alerta = "Campos incorrectos" });
        }

        [HttpPost]
        public async Task<IActionResult> Edit(int id, [Bind("ExpensaId,ExpensaPdf,ExpensaPeriodo")] Expensa expensa)
        {
            if (id != expensa.ExpensaId)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(expensa);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!ExpensaExists(expensa.ExpensaId))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return Json(true);
            }
            return View(expensa);
        }

        public async Task<IActionResult> Delete(int? id)
        {
            var expensa = await _context.Expensa.FindAsync(id);
            _context.Expensa.Remove(expensa);
            await _context.SaveChangesAsync();
            return Json(new { eliminado = true, msg = "Expensa eliminada" });
        }

        private bool ExpensaExists(int id)
        {
            return _context.Expensa.Any(e => e.ExpensaId == id);
        }
    }
}
