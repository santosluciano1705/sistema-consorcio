﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using SistemaConsorcio.Data;
using SistemaConsorcio.Models;

namespace SistemaConsorcio.Controllers
{
    [Authorize]
    public class HomeController : Controller
    {
        private readonly ApplicationDbContext _context;

        public HomeController(ApplicationDbContext context)
        {
            _context = context;
        }
        public IActionResult Index()
        {
            General general = _context.General.FirstOrDefault();
            if (general != null)
                this.ViewData["nombre_consorcio"] = general.GeneralNombreConsorcio;
            else
                this.ViewData["nombre_consorcio"] = "Consorcio";
            return View();
        }

        public IActionResult getInicio()
        {
            return PartialView("_inicio");
        }

        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
