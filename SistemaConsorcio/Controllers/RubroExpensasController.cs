﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using SistemaConsorcio.Data;
using SistemaConsorcio.Models;

namespace SistemaConsorcio.Controllers
{
    [Authorize]
    public class RubroExpensasController : Controller
    {
        private readonly ApplicationDbContext _context;

        public RubroExpensasController(ApplicationDbContext context)
        {
            _context = context;
        }

        // GET: RubroExpensas
        public async Task<IActionResult> Index()
        {
            return View(await _context.RubroExpensa.ToListAsync());
        }

        public async Task<IActionResult> getRubroExpensas()
        {
            return PartialView("_Index",await _context.RubroExpensa.OrderBy(r => r.RubroExpensaDescripcion).ToListAsync());
        }

        // GET: RubroExpensas/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var rubroExpensa = await _context.RubroExpensa
                .FirstOrDefaultAsync(m => m.RubroExpensaId == id);
            if (rubroExpensa == null)
            {
                return NotFound();
            }

            return View(rubroExpensa);
        }

        // GET: RubroExpensas/Create
        public IActionResult Create()
        {
            return PartialView("_Create");
        }

        [HttpPost]
        public IActionResult Agregar([Bind("RubroExpensaId,RubroExpensaDescripcion,RubroExpensaInformacion")] RubroExpensa rubroExpensa)
        {
            if (ModelState.IsValid)
            {
                _context.Add(rubroExpensa);
                _context.SaveChanges();
                return Json(new { exitoso = true, alerta = "Agregado correctamente" });
            }
            else
            {
                return Json(new { exitoso = false, alerta = "Error al agregar Rubro" });
            }
        }

        // POST: RubroExpensas/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("RubroExpensaId,RubroExpensaDescripcion,RubroExpensaInformacion")] RubroExpensa rubroExpensa)
        {
            if (ModelState.IsValid)
            {
                _context.Add(rubroExpensa);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(rubroExpensa);
        }

        // GET: RubroExpensas/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var rubroExpensa = await _context.RubroExpensa.FindAsync(id);
            if (rubroExpensa == null)
            {
                return NotFound();
            }
            return View(rubroExpensa);
        }

        // POST: RubroExpensas/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("RubroExpensaId,RubroExpensaDescripcion,RubroExpensaInformacion")] RubroExpensa rubroExpensa)
        {
            if (id != rubroExpensa.RubroExpensaId)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(rubroExpensa);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!RubroExpensaExists(rubroExpensa.RubroExpensaId))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(rubroExpensa);
        }

        // GET: RubroExpensas/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            var cantRubros = _context.Movimientos.Where(m => m.RubroExpensaId == id).Count();
            if (cantRubros == 0)
            {
                var rubroExpensa = await _context.RubroExpensa.FindAsync(id);
                _context.RubroExpensa.Remove(rubroExpensa);
                await _context.SaveChangesAsync();
                return Json(new { eliminado = true, msg = "Rubro eliminado" });
            }
            else
            {
                return Json(new { eliminado = false, msg = "No se puede eliminar. Tiene movimientos asociados" });
            }

        }

        // POST: RubroExpensas/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var rubroExpensa = await _context.RubroExpensa.FindAsync(id);
            _context.RubroExpensa.Remove(rubroExpensa);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool RubroExpensaExists(int id)
        {
            return _context.RubroExpensa.Any(e => e.RubroExpensaId == id);
        }

        public async Task<IActionResult> getRubroExpensa(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }
            var rubroExpensa = await _context.RubroExpensa.FindAsync(id);
            if (rubroExpensa == null)
            {
                return NotFound();
            }
            return PartialView("_Edit", rubroExpensa);
        }
        public ActionResult ActualizarRubroExpensa(RubroExpensa rubroExpensa)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    RubroExpensa updateRubro = _context.RubroExpensa.Find(rubroExpensa.RubroExpensaId);
                    updateRubro.RubroExpensaDescripcion = rubroExpensa.RubroExpensaDescripcion;
                    _context.SaveChanges();
                    return Json(new { exitoso = true, alerta = "Actualizado correctamente" });
                }
                catch (DbUpdateConcurrencyException)
                {
                    return Json(new { exitoso = false, alerta = "Error en la actualizacion" });
                }
            }
            return Json(new { exitoso = true, alerta = "Campos incorrectos" });
        }
    }
}
