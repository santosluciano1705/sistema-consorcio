﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using SistemaConsorcio.Data;
using SistemaConsorcio.Models;

namespace SistemaConsorcio.Controllers
{
    [Authorize]
    public class GeneralesController : Controller
    {
        private readonly ApplicationDbContext _context;

        public GeneralesController(ApplicationDbContext context)
        {
            _context = context;
        }

        // GET: Generales
        public async Task<IActionResult> Index()
        {
            return View(await _context.General.ToListAsync());
        }
        public async Task<JsonResult> generarConfiguracion()
        {
            General primero = new General();
            primero.GeneralPrimerVencimiento = 0;
            primero.GeneralSegundoVencimiento = 0;
            primero.GeneralTercerVencimiento = 0;
            primero.GeneralExpensaActiva = 0;
            await this.Create(primero);
            return Json(true);
        }
        public async Task<IActionResult> getConfiguracion()
        {
            var configuracion = await _context.General.FirstOrDefaultAsync();
            if (configuracion == null)
            {
                General primero = new General();
                primero.GeneralPrimerVencimiento = 0;
                primero.GeneralSegundoVencimiento = 0;
                primero.GeneralTercerVencimiento = 0;
                primero.GeneralExpensaActiva = 0;
                await this.Create(primero);
                return PartialView("_index", await _context.General.FirstAsync());
            }
            else
            {
                return PartialView("_index", await _context.General.FirstAsync());
            }
        }

        // POST: Generales/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("GeneralId,GeneralNombreAdministracion,GeneralNombreConsorcio,GeneralNombreCuentaBancaria,GeneralMailFrom,GeneralMailPassword,GeneralPrimerVencimiento,GeneralSegundoVencimiento,GeneralTercerVencimiento,GeneralPorcentajeCochera")] General general)
        {
            if (ModelState.IsValid)
            {
                _context.Add(general);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(general);
        }
        private bool GeneralExists(int id)
        {
            return _context.General.Any(e => e.GeneralId == id);
        }
        public void enviarMailAdjunto(byte[] bytes, string destinatario, string asunto, string body, string nombre_archivo)
        {
            var datos = _context.General.ToList().First();
            var fromAddress = new MailAddress(datos.GeneralMailFrom,datos.GeneralNombreAdministracion, System.Text.Encoding.UTF8);//quien manda
            var toAddress = new MailAddress(destinatario);//el que recibe 
            string fromPassword = datos.GeneralMailPassword;//password mail del que manda
            var smtp = new SmtpClient
            {
                Host = "smtp.gmail.com",
                Port = 587,
                EnableSsl = true,
                DeliveryMethod = SmtpDeliveryMethod.Network,
                UseDefaultCredentials = false,
                Credentials = new NetworkCredential(fromAddress.Address, fromPassword)
            };
            using (var message = new MailMessage(fromAddress, toAddress)
            {
                Subject = asunto,
                Body = body
            })
            {
                Attachment att = new Attachment(new MemoryStream(bytes), nombre_archivo + ".pdf");//nombre del archivo que envio es el segundo parametro, tiene que ser .pdf
                message.Attachments.Add(att);
                message.Priority = MailPriority.High;
                smtp.Send(message);
            }
        }

        [HttpPost]
        public ActionResult ActualizarConfiguracion([Bind("GeneralId,GeneralNombreAdministracion,GeneralNombreConsorcio,GeneralNombreCuentaBancaria,GeneralMailFrom,GeneralMailPassword,GeneralPrimerVencimiento,GeneralSegundoVencimiento,GeneralTercerVencimiento,GeneralPorcentajeCochera,GeneralDiaPrimerVencimiento,GeneralDiaSegundoVencimiento,GeneralDiaTercerVencimiento,GeneralTelefonoAdministracion,GeneralTelefonoAdministracionCodigo,GeneralDireccionAdministracion,GeneralEnvioMails,GeneralVencimientoHabiles")] General configuracion)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    General updateGeneral = _context.General.Find(configuracion.GeneralId);
                    if (!string.IsNullOrWhiteSpace(configuracion.GeneralNombreAdministracion)) { updateGeneral.GeneralNombreAdministracion = configuracion.GeneralNombreAdministracion; }
                    if (!string.IsNullOrWhiteSpace(configuracion.GeneralMailFrom)) { updateGeneral.GeneralMailFrom = configuracion.GeneralMailFrom; }
                    if (!string.IsNullOrWhiteSpace(configuracion.GeneralNombreConsorcio)) { updateGeneral.GeneralNombreConsorcio = configuracion.GeneralNombreConsorcio; }
                    if (!string.IsNullOrWhiteSpace(configuracion.GeneralNombreCuentaBancaria)) { updateGeneral.GeneralNombreCuentaBancaria = configuracion.GeneralNombreCuentaBancaria; }
                    if (!string.IsNullOrWhiteSpace(configuracion.GeneralMailPassword)) { updateGeneral.GeneralMailPassword = configuracion.GeneralMailPassword; }
                    if (!string.IsNullOrWhiteSpace(configuracion.GeneralPrimerVencimiento.ToString())) { updateGeneral.GeneralPrimerVencimiento = configuracion.GeneralPrimerVencimiento; }
                    if (!string.IsNullOrWhiteSpace(configuracion.GeneralSegundoVencimiento.ToString())) { updateGeneral.GeneralSegundoVencimiento = configuracion.GeneralSegundoVencimiento; }
                    if (!string.IsNullOrWhiteSpace(configuracion.GeneralTercerVencimiento.ToString())) { updateGeneral.GeneralTercerVencimiento = configuracion.GeneralTercerVencimiento; }
                    if (!string.IsNullOrWhiteSpace(configuracion.GeneralPorcentajeCochera.ToString())) { updateGeneral.GeneralPorcentajeCochera = configuracion.GeneralPorcentajeCochera; }
                    if (!string.IsNullOrWhiteSpace(configuracion.GeneralDiaPrimerVencimiento.ToString())) { updateGeneral.GeneralDiaPrimerVencimiento = configuracion.GeneralDiaPrimerVencimiento; }
                    if (!string.IsNullOrWhiteSpace(configuracion.GeneralDiaSegundoVencimiento.ToString())) { updateGeneral.GeneralDiaSegundoVencimiento = configuracion.GeneralDiaSegundoVencimiento; }
                    if (!string.IsNullOrWhiteSpace(configuracion.GeneralDiaTercerVencimiento.ToString())) { updateGeneral.GeneralDiaTercerVencimiento = configuracion.GeneralDiaTercerVencimiento; }
                    if (!string.IsNullOrWhiteSpace(configuracion.GeneralTelefonoAdministracion.ToString())) { updateGeneral.GeneralTelefonoAdministracion = configuracion.GeneralTelefonoAdministracion; }
                    if (!string.IsNullOrWhiteSpace(configuracion.GeneralTelefonoAdministracionCodigo.ToString())) { updateGeneral.GeneralTelefonoAdministracionCodigo = configuracion.GeneralTelefonoAdministracionCodigo; }
                    if (!string.IsNullOrWhiteSpace(configuracion.GeneralDireccionAdministracion)) { updateGeneral.GeneralDireccionAdministracion = configuracion.GeneralDireccionAdministracion; }
                    updateGeneral.GeneralEnvioMails = configuracion.GeneralEnvioMails;
                    updateGeneral.GeneralVencimientoHabiles = configuracion.GeneralVencimientoHabiles;
                    _context.SaveChanges();
                    return Json(new { exitoso = true, alerta = "Actualizado correctamente" });
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!GeneralExists(configuracion.GeneralId))
                    {
                        return Json(new { exitoso = false, alerta = "Error en la actualizacion" });
                    }
                    else
                    {
                        return Json(new { exitoso = false, alerta = "Error en la actualizacion" });
                    }
                }
            }
            return Json(new { exitoso = false, alerta = "Error, verifique los campos ingresados" });
        }
    }
}
